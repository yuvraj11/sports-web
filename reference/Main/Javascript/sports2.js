function onYouTubeIframeAPIReady() {
    $(".player").each(function(t) {
        $(this).attr("data-videocount", t);
        var e = new YT.Player($(this)[0], {
            height: "390",
            width: "640",
            rel: 0,
            videoId: $(this).attr("data-videoid"),
            events: {
                onReady: onPlayerReady,
                onStateChange: onPlayerStateChange
            }
        });
        $(this).attr("data-mute"), players.push(e)
    })
}

function onPlayerStateChange(t) {
    0 == t.data && "true" == $(t.target.a).attr("data-autoplay") && t.target.playVideo()
}! function(t, e) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function(t) {
        if (!t.document) throw new Error("jQuery requires a window with a document");
        return e(t)
    } : e(t)
}("undefined" != typeof window ? window : this, function(t, e) {
    "use strict";

    function n(t) {
        var e = !!t && "length" in t && t.length,
            i = at.type(t);
        return "function" !== i && !at.isWindow(t) && ("array" === i || 0 === e || "number" == typeof e && e > 0 && e - 1 in t)
    }

    function r(t, e) {}

    function s(t, e, i) {}

    function l(t) {
        throw t
    }

    function c() {}

    function u() {
        this.expando = at.expando + u.uid++
    }

    function m(t, e) {
        for (var i, n, r = [], s = 0, o = t.length; s < o; s++)(n = t[s]).style && (i = n.style.display, e ? ("none" === i && (r[s] = Ct.get(n, "display") || null, r[s] || (n.style.display = "")), "" === n.style.display && $t(n) && (r[s] = f(n))) : "none" !== i && (r[s] = "none", Ct.set(n, "display", i)));
        for (s = 0; s < o; s++) null != r[s] && (t[s].style.display = r[s]);
        return t
    }

    function _(t, e, i, n, r) {
        for (var s, o, a, l, h, c, u = e.createDocumentFragment(), p = [], d = 0, f = t.length; d < f; d++)
            if ((s = t[d]) || 0 === s)
                if ("object" === at.type(s)) at.merge(p, s.nodeType ? [s] : s);
                else if (Lt.test(s)) {
            for (o = o || u.appendChild(e.createElement("div")), a = (Nt.exec(s) || ["", ""])[1].toLowerCase(), l = jt[a] || jt._default, o.innerHTML = l[1] + at.htmlPrefilter(s) + l[2], c = l[0]; c--;) o = o.lastChild;
            at.merge(p, o.childNodes), (o = u.firstChild).textContent = ""
        } else p.push(e.createTextNode(s));
        for (u.textContent = "", d = 0; s = p[d++];)
            if (n && at.inArray(s, n) > -1) r && r.push(s);
            else if (h = at.contains(s.ownerDocument, s), o = g(u.appendChild(s), "script"), h && v(o), i)
            for (c = 0; s = o[c++];) zt.test(s.type || "") && i.push(s);
        return u
    }

    function y() {
        return !0
    }

    function w() {
        return !1
    }

    function x() {
        try {
            return Q.activeElement
        } catch (t) {}
    }

    function b(t, e, i, n, r, s) {
        var o, a;
        if ("object" == typeof e) {
            for (a in "string" != typeof i && (n = n || i, i = void 0), e) b(t, a, i, n, e[a], s);
            return t
        }
        if (null == n && null == r ? (r = i, n = i = void 0) : null == r && ("string" == typeof i ? (r = n, n = void 0) : (r = n, n = i, i = void 0)), !1 === r) r = w;
        else if (!r) return t;
        return 1 === s && (o = r, (r = function(t) {
            return at().off(t), o.apply(this, arguments)
        }).guid = o.guid || (o.guid = at.guid++)), t.each(function() {
            at.event.add(this, e, r, n, i)
        })
    }

    function T(t, e) {
        return r(t, "table") && r(11 !== e.nodeType ? e : e.firstChild, "tr") && at(">tbody", t)[0] || t
    }

    function k(t, e) {
        var i = e.nodeName.toLowerCase();
        "input" === i && Mt.test(t.type) ? e.checked = t.checked : "input" !== i && "textarea" !== i || (e.defaultValue = t.defaultValue)
    }

    function $(t, e) {

    }

    function z(t, e, i, n, r) {}

    function F(t, e) {
        var i, n = 0,
            r = {
                height: t
            };
        for (e = e ? 1 : 0; n < 4; n += 2 - e) r["margin" + (i = Et[n])] = r["padding" + i] = t;
        return e && (r.opacity = r.width = t), r
    }

    function q(t, e, i) {}

    function H(t) {
        return (t.match(yt) || []).join(" ")
    }

    function B(t) {
        return t.getAttribute && t.getAttribute("class") || ""
    }

    function W(t) {
        return function(e, i) {
            "string" != typeof e && (i = e, e = "*");
            var n, r = 0,
                s = e.toLowerCase().match(yt) || [];
            if (at.isFunction(i))
                for (; n = s[r++];) "+" === n[0] ? (n = n.slice(1) || "*", (t[n] = t[n] || []).unshift(i)) : (t[n] = t[n] || []).push(i)
        }
    }


    function U(t, e) {}
    var V = [],
        Q = t.document,
        G = Object.getPrototypeOf,
        Z = V.slice,
        J = V.concat,
        K = V.push,
        tt = V.indexOf,
        et = {},
        it = et.toString,
        nt = et.hasOwnProperty,
        rt = nt.toString,
        st = rt.call(Object),
        ot = {},
        at = function(t, e) {
            return new at.fn.init(t, e)
        },
        lt = function(t, e) {
            return e.toUpperCase()
        };
    at.fn = at.prototype = {
        jquery: "3.2.1",
        constructor: at,
        length: 0,
        toArray: function() {
            return Z.call(this)
        },
        get: function(t) {
            return null == t ? Z.call(this) : t < 0 ? this[t + this.length] : this[t]
        },
        pushStack: function(t) {
            var e = at.merge(this.constructor(), t);
            return e.prevObject = this, e
        },
        each: function(t) {
            return at.each(this, t)
        },
        map: function(t) {
            return this.pushStack(at.map(this, function(e, i) {
                return t.call(e, i, e)
            }))
        },
        slice: function() {
            return this.pushStack(Z.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(t) {
            var e = this.length,
                i = +t + (t < 0 ? e : 0);
            return this.pushStack(i >= 0 && i < e ? [this[i]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: K,
        sort: V.sort,
        splice: V.splice
    }, at.extend = at.fn.extend = function() {
        var t, e, i, n, r, s, o = arguments[0] || {},
            a = 1,
            l = arguments.length,
            h = !1;
        for ("boolean" == typeof o && (h = o, o = arguments[a] || {}, a++), "object" == typeof o || at.isFunction(o) || (o = {}), a === l && (o = this, a--); a < l; a++)
            if (null != (t = arguments[a]))
                for (e in t) i = o[e], o !== (n = t[e]) && (h && n && (at.isPlainObject(n) || (r = Array.isArray(n))) ? (r ? (r = !1, s = i && Array.isArray(i) ? i : []) : s = i && at.isPlainObject(i) ? i : {}, o[e] = at.extend(h, s, n)) : void 0 !== n && (o[e] = n));
        return o
    }, at.extend({
        expando: "jQuery" + ("3.2.1" + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(t) {
            throw new Error(t)
        },
        noop: function() {},
        isFunction: function(t) {
            return "function" === at.type(t)
        },
        isWindow: function(t) {
            return null != t && t === t.window
        },
        isNumeric: function(t) {
            var e = at.type(t);
            return ("number" === e || "string" === e) && !isNaN(t - parseFloat(t))
        },
        isPlainObject: function(t) {
            var e, i;
            return !(!t || "[object Object]" !== it.call(t) || (e = G(t)) && ("function" != typeof(i = nt.call(e, "constructor") && e.constructor) || rt.call(i) !== st))
        },
        isEmptyObject: function(t) {
            var e;
            for (e in t) return !1;
            return !0
        },
        type: function(t) {
            return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? et[it.call(t)] || "object" : typeof t
        },
        globalEval: function(t) {
            i(t)
        },
        camelCase: function(t) {
            return t.replace(/^-ms-/, "ms-").replace(/-([a-z])/g, lt)
        },
        each: function(t, e) {
            var i, r = 0;
            if (n(t))
                for (i = t.length; r < i && !1 !== e.call(t[r], r, t[r]); r++);
            else
                for (r in t)
                    if (!1 === e.call(t[r], r, t[r])) break; return t
        },
        trim: function(t) {
            return null == t ? "" : (t + "").replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "")
        },
        makeArray: function(t, e) {
            var i = e || [];
            return null != t && (n(Object(t)) ? at.merge(i, "string" == typeof t ? [t] : t) : K.call(i, t)), i
        },
        inArray: function(t, e, i) {
            return null == e ? -1 : tt.call(e, t, i)
        },
        merge: function(t, e) {
            for (var i = +e.length, n = 0, r = t.length; n < i; n++) t[r++] = e[n];
            return t.length = r, t
        },
        grep: function(t, e, i) {
            for (var n = [], r = 0, s = t.length, o = !i; r < s; r++) !e(t[r], r) !== o && n.push(t[r]);
            return n
        },
        map: function(t, e, i) {
            var r, s, o = 0,
                a = [];
            if (n(t))
                for (r = t.length; o < r; o++) null != (s = e(t[o], o, i)) && a.push(s);
            else
                for (o in t) null != (s = e(t[o], o, i)) && a.push(s);
            return J.apply([], a)
        },
        guid: 1,
        proxy: function(t, e) {
            var i, n, r;
            if ("string" == typeof e && (i = t[e], e = t, t = i), at.isFunction(t)) return n = Z.call(arguments, 2), (r = function() {
                return t.apply(e || this, n.concat(Z.call(arguments)))
            }).guid = t.guid = t.guid || at.guid++, r
        },
        now: Date.now,
        support: ot
    }), "function" == typeof Symbol && (at.fn[Symbol.iterator] = V[Symbol.iterator]), at.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(t, e) {
        et["[object " + e + "]"] = e.toLowerCase()
    });
    var ht = function(t) {
        function e(t, e, i, n) {
            var r, s, o, a, l, c, p, d = e && e.ownerDocument,
                f = e ? e.nodeType : 9;
            if (i = i || [], "string" != typeof t || !t || 1 !== f && 9 !== f && 11 !== f) return i;
            if (!n && ((e ? e.ownerDocument || e : F) !== $ && E(e), e = e || $, R)) {
                if (11 !== f && (l = mt.exec(t)))
                    if (r = l[1]) {
                        if (9 === f) {
                            if (!(o = e.getElementById(r))) return i;
                            if (o.id === r) return i.push(o), i
                        } else if (d && (o = d.getElementById(r)) && j(e, o) && o.id === r) return i.push(o), i
                    } else {
                        if (l[2]) return G.apply(i, e.getElementsByTagName(t)), i;
                        if ((r = l[3]) && w.getElementsByClassName && e.getElementsByClassName) return G.apply(i, e.getElementsByClassName(r)), i
                    }
                if (w.qsa && !X[t + " "] && (!M || !M.test(t))) {
                    if (1 !== f) d = e, p = t;
                    else if ("object" !== e.nodeName.toLowerCase()) {
                        for ((a = e.getAttribute("id")) ? a = a.replace(yt, wt) : e.setAttribute("id", a = L), s = (c = C(t)).length; s--;) c[s] = "#" + a + " " + u(c[s]);
                        p = c.join(","), d = gt.test(t) && h(e.parentNode) || e
                    }
                    if (p) try {
                        return G.apply(i, d.querySelectorAll(p)), i
                    } catch (t) {} finally {
                        a === L && e.removeAttribute("id")
                    }
                }
            }
            return P(t.replace(st, "$1"), e, i, n)
        }

        function i() {
            var t = [];
            return function e(i, n) {
                return t.push(i + " ") > x.cacheLength && delete e[t.shift()], e[i + " "] = n
            }
        }

        function n(t) {
            return t[L] = !0, t
        }

        function r(t) {
            var e = $.createElement("fieldset");
            try {
                return !!t(e)
            } catch (t) {
                return !1
            } finally {
                e.parentNode && e.parentNode.removeChild(e), e = null
            }
        }

        function s(t, e) {
            for (var i = t.split("|"), n = i.length; n--;) x.attrHandle[i[n]] = e
        }

        function o(t, e) {
            var i = e && t,
                n = i && 1 === t.nodeType && 1 === e.nodeType && t.sourceIndex - e.sourceIndex;
            if (n) return n;
            if (i)
                for (; i = i.nextSibling;)
                    if (i === e) return -1;
            return t ? 1 : -1
        }

        function a(t) {
            return function(e) {
                return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && bt(e) === t : e.disabled === t : "label" in e && e.disabled === t
            }
        }

        function l(t) {
            return n(function(e) {
                return e = +e, n(function(i, n) {
                    for (var r, s = t([], i.length, e), o = s.length; o--;) i[r = s[o]] && (i[r] = !(n[r] = i[r]))
                })
            })
        }

        function h(t) {
            return t && void 0 !== t.getElementsByTagName && t
        }

        function c() {}

        function u(t) {
            for (var e = 0, i = t.length, n = ""; e < i; e++) n += t[e].value;
            return n
        }

        function p(t, e, i) {
            var n = e.dir,
                r = e.next,
                s = r || n,
                o = i && "parentNode" === s,
                a = q++;
            return e.first ? function(e, i, r) {
                for (; e = e[n];)
                    if (1 === e.nodeType || o) return t(e, i, r);
                return !1
            } : function(e, i, l) {
                var h, c, u, p = [I, a];
                if (l) {
                    for (; e = e[n];)
                        if ((1 === e.nodeType || o) && t(e, i, l)) return !0
                } else
                    for (; e = e[n];)
                        if (1 === e.nodeType || o)
                            if (c = (u = e[L] || (e[L] = {}))[e.uniqueID] || (u[e.uniqueID] = {}), r && r === e.nodeName.toLowerCase()) e = e[n] || e;
                            else {
                                if ((h = c[s]) && h[0] === I && h[1] === a) return p[2] = h[2];
                                if (c[s] = p, p[2] = t(e, i, l)) return !0
                            } return !1
            }
        }

        function d(t) {
            return t.length > 1 ? function(e, i, n) {
                for (var r = t.length; r--;)
                    if (!t[r](e, i, n)) return !1;
                return !0
            } : t[0]
        }

        function f(t, i, n) {
            for (var r = 0, s = i.length; r < s; r++) e(t, i[r], n);
            return n
        }

        function m(t, e, i, n, r) {
            for (var s, o = [], a = 0, l = t.length, h = null != e; a < l; a++)(s = t[a]) && (i && !i(s, n, r) || (o.push(s), h && e.push(a)));
            return o
        }

        function g(t, e, i, r, s, o) {
            return r && !r[L] && (r = g(r)), s && !s[L] && (s = g(s, o)), n(function(n, o, a, l) {
                var h, c, u, p = [],
                    d = [],
                    g = o.length,
                    v = n || f(e || "*", a.nodeType ? [a] : a, []),
                    _ = !t || !n && e ? v : m(v, p, t, a, l),
                    y = i ? s || (n ? t : g || r) ? [] : o : _;
                if (i && i(_, y, a, l), r)
                    for (h = m(y, d), r(h, [], a, l), c = h.length; c--;)(u = h[c]) && (y[d[c]] = !(_[d[c]] = u));
                if (n) {
                    if (s || t) {
                        if (s) {
                            for (h = [], c = y.length; c--;)(u = y[c]) && h.push(_[c] = u);
                            s(null, y = [], h, l)
                        }
                        for (c = y.length; c--;)(u = y[c]) && (h = s ? J(n, u) : p[c]) > -1 && (n[h] = !(o[h] = u))
                    }
                } else y = m(y === o ? y.splice(g, y.length) : y), s ? s(null, o, y, l) : G.apply(o, y)
            })
        }

        function v(t) {
            for (var e, i, n, r = t.length, s = x.relative[t[0].type], o = s || x.relative[" "], a = s ? 1 : 0, l = p(function(t) {
                    return t === e
                }, o, !0), h = p(function(t) {
                    return J(e, t) > -1
                }, o, !0), c = [function(t, i, n) {
                    var r = !s && (n || i !== k) || ((e = i).nodeType ? l(t, i, n) : h(t, i, n));
                    return e = null, r
                }]; a < r; a++)
                if (i = x.relative[t[a].type]) c = [p(d(c), i)];
                else {
                    if ((i = x.filter[t[a].type].apply(null, t[a].matches))[L]) {
                        for (n = ++a; n < r && !x.relative[t[n].type]; n++);
                        return g(a > 1 && d(c), a > 1 && u(t.slice(0, a - 1).concat({
                            value: " " === t[a - 2].type ? "*" : ""
                        })).replace(st, "$1"), i, a < n && v(t.slice(a, n)), n < r && v(t = t.slice(n)), n < r && u(t))
                    }
                    c.push(i)
                }
            return d(c)
        }

        function _(t, i) {
            var r = i.length > 0,
                s = t.length > 0,
                o = function(n, o, a, l, h) {
                    var c, u, p, d = 0,
                        f = "0",
                        g = n && [],
                        v = [],
                        _ = k,
                        y = n || s && x.find.TAG("*", h),
                        w = I += null == _ ? 1 : Math.random() || .1,
                        b = y.length;
                    for (h && (k = o === $ || o || h); f !== b && null != (c = y[f]); f++) {
                        if (s && c) {
                            for (u = 0, o || c.ownerDocument === $ || (E(c), a = !R); p = t[u++];)
                                if (p(c, o || $, a)) {
                                    l.push(c);
                                    break
                                }
                            h && (I = w)
                        }
                        r && ((c = !p && c) && d--, n && g.push(c))
                    }
                    if (d += f, r && f !== d) {
                        for (u = 0; p = i[u++];) p(g, v, o, a);
                        if (n) {
                            if (d > 0)
                                for (; f--;) g[f] || v[f] || (v[f] = V.call(l));
                            v = m(v)
                        }
                        G.apply(l, v), h && !n && v.length > 0 && d + i.length > 1 && e.uniqueSort(l)
                    }
                    return h && (I = w, k = _), g
                };
            return r ? n(o) : o
        }
        var y, w, x, b, T, C, S, P, k, A, D, E, $, O, R, M, N, z, j, L = "sizzle" + 1 * new Date,
            F = t.document,
            I = 0,
            q = 0,
            H = i(),
            B = i(),
            X = i(),
            W = function(t, e) {
                return t === e && (D = !0), 0
            },
            Y = {}.hasOwnProperty,
            U = [],
            V = U.pop,
            Q = U.push,
            G = U.push,
            Z = U.slice,
            J = function(t, e) {
                for (var i = 0, n = t.length; i < n; i++)
                    if (t[i] === e) return i;
                return -1
            },
            K = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            tt = "[\\x20\\t\\r\\n\\f]",
            et = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            it = "\\[" + tt + "*(" + et + ")(?:" + tt + "*([*^$|!~]?=)" + tt + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + et + "))|)" + tt + "*\\]",
            nt = ":(" + et + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + it + ")*)|.*)\\)|)",
            rt = new RegExp(tt + "+", "g"),
            st = new RegExp("^" + tt + "+|((?:^|[^\\\\])(?:\\\\.)*)" + tt + "+$", "g"),
            ot = new RegExp("^" + tt + "*," + tt + "*"),
            at = new RegExp("^" + tt + "*([>+~]|" + tt + ")" + tt + "*"),
            lt = new RegExp("=" + tt + "*([^\\]'\"]*?)" + tt + "*\\]", "g"),
            ht = new RegExp(nt),
            ct = new RegExp("^" + et + "$"),
            ut = {
                ID: new RegExp("^#(" + et + ")"),
                CLASS: new RegExp("^\\.(" + et + ")"),
                TAG: new RegExp("^(" + et + "|[*])"),
                ATTR: new RegExp("^" + it),
                PSEUDO: new RegExp("^" + nt),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + tt + "*(even|odd|(([+-]|)(\\d*)n|)" + tt + "*(?:([+-]|)" + tt + "*(\\d+)|))" + tt + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + K + ")$", "i"),
                needsContext: new RegExp("^" + tt + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + tt + "*((?:-\\d)?\\d*)" + tt + "*\\)|)(?=[^-]|$)", "i")
            },
            pt = /^(?:input|select|textarea|button)$/i,
            dt = /^h\d$/i,
            ft = /^[^{]+\{\s*\[native \w/,
            mt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            gt = /[+~]/,
            vt = new RegExp("\\\\([\\da-f]{1,6}" + tt + "?|(" + tt + ")|.)", "ig"),
            _t = function(t, e, i) {
                var n = "0x" + e - 65536;
                return n != n || i ? e : n < 0 ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320)
            },
            yt = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            wt = function(t, e) {
                return e ? "\0" === t ? "ï¿½" : t.slice(0, -1) + "\\" + t.charCodeAt(t.length - 1).toString(16) + " " : "\\" + t
            },
            xt = function() {
                E()
            },
            bt = p(function(t) {
                return !0 === t.disabled && ("form" in t || "label" in t)
            }, {
                dir: "parentNode",
                next: "legend"
            });
        try {
            G.apply(U = Z.call(F.childNodes), F.childNodes), U[F.childNodes.length].nodeType
        } catch (t) {
            G = {
                apply: U.length ? function(t, e) {
                    Q.apply(t, Z.call(e))
                } : function(t, e) {
                    for (var i = t.length, n = 0; t[i++] = e[n++];);
                    t.length = i - 1
                }
            }
        }
        for (y in w = e.support = {}, T = e.isXML = function(t) {
                var e = t && (t.ownerDocument || t).documentElement;
                return !!e && "HTML" !== e.nodeName
            }, E = e.setDocument = function(t) {
                var e, i, n = t ? t.ownerDocument || t : F;
                return n !== $ && 9 === n.nodeType && n.documentElement ? (O = ($ = n).documentElement, R = !T($), F !== $ && (i = $.defaultView) && i.top !== i && (i.addEventListener ? i.addEventListener("unload", xt, !1) : i.attachEvent && i.attachEvent("onunload", xt)), w.attributes = r(function(t) {
                    return t.className = "i", !t.getAttribute("className")
                }), w.getElementsByTagName = r(function(t) {
                    return t.appendChild($.createComment("")), !t.getElementsByTagName("*").length
                }), w.getElementsByClassName = ft.test($.getElementsByClassName), w.getById = r(function(t) {
                    return O.appendChild(t).id = L, !$.getElementsByName || !$.getElementsByName(L).length
                }), w.getById ? (x.filter.ID = function(t) {
                    var e = t.replace(vt, _t);
                    return function(t) {
                        return t.getAttribute("id") === e
                    }
                }, x.find.ID = function(t, e) {
                    if (void 0 !== e.getElementById && R) {
                        var i = e.getElementById(t);
                        return i ? [i] : []
                    }
                }) : (x.filter.ID = function(t) {
                    var e = t.replace(vt, _t);
                    return function(t) {
                        var i = void 0 !== t.getAttributeNode && t.getAttributeNode("id");
                        return i && i.value === e
                    }
                }, x.find.ID = function(t, e) {
                    if (void 0 !== e.getElementById && R) {
                        var i, n, r, s = e.getElementById(t);
                        if (s) {
                            if ((i = s.getAttributeNode("id")) && i.value === t) return [s];
                            for (r = e.getElementsByName(t), n = 0; s = r[n++];)
                                if ((i = s.getAttributeNode("id")) && i.value === t) return [s]
                        }
                        return []
                    }
                }), x.find.TAG = w.getElementsByTagName ? function(t, e) {
                    return void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t) : w.qsa ? e.querySelectorAll(t) : void 0
                } : function(t, e) {
                    var i, n = [],
                        r = 0,
                        s = e.getElementsByTagName(t);
                    if ("*" === t) {
                        for (; i = s[r++];) 1 === i.nodeType && n.push(i);
                        return n
                    }
                    return s
                }, x.find.CLASS = w.getElementsByClassName && function(t, e) {
                    if (void 0 !== e.getElementsByClassName && R) return e.getElementsByClassName(t)
                }, N = [], M = [], (w.qsa = ft.test($.querySelectorAll)) && (r(function(t) {
                    O.appendChild(t).innerHTML = "<a id='" + L + "'></a><select id='" + L + "-\r\\' msallowcapture=''><option selected=''></option></select>", t.querySelectorAll("[msallowcapture^='']").length && M.push("[*^$]=" + tt + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || M.push("\\[" + tt + "*(?:value|" + K + ")"), t.querySelectorAll("[id~=" + L + "-]").length || M.push("~="), t.querySelectorAll(":checked").length || M.push(":checked"), t.querySelectorAll("a#" + L + "+*").length || M.push(".#.+[+~]")
                }), r(function(t) {
                    t.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                    var e = $.createElement("input");
                    e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && M.push("name" + tt + "*[*^$|!~]?="), 2 !== t.querySelectorAll(":enabled").length && M.push(":enabled", ":disabled"), O.appendChild(t).disabled = !0, 2 !== t.querySelectorAll(":disabled").length && M.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), M.push(",.*:")
                })), (w.matchesSelector = ft.test(z = O.matches || O.webkitMatchesSelector || O.mozMatchesSelector || O.oMatchesSelector || O.msMatchesSelector)) && r(function(t) {
                    w.disconnectedMatch = z.call(t, "*"), z.call(t, "[s!='']:x"), N.push("!=", nt)
                }), M = M.length && new RegExp(M.join("|")), N = N.length && new RegExp(N.join("|")), e = ft.test(O.compareDocumentPosition), j = e || ft.test(O.contains) ? function(t, e) {
                    var i = 9 === t.nodeType ? t.documentElement : t,
                        n = e && e.parentNode;
                    return t === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(n)))
                } : function(t, e) {
                    if (e)
                        for (; e = e.parentNode;)
                            if (e === t) return !0;
                    return !1
                }, W = e ? function(t, e) {
                    if (t === e) return D = !0, 0;
                    var i = !t.compareDocumentPosition - !e.compareDocumentPosition;
                    return i || (1 & (i = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1) || !w.sortDetached && e.compareDocumentPosition(t) === i ? t === $ || t.ownerDocument === F && j(F, t) ? -1 : e === $ || e.ownerDocument === F && j(F, e) ? 1 : A ? J(A, t) - J(A, e) : 0 : 4 & i ? -1 : 1)
                } : function(t, e) {
                    if (t === e) return D = !0, 0;
                    var i, n = 0,
                        r = t.parentNode,
                        s = e.parentNode,
                        a = [t],
                        l = [e];
                    if (!r || !s) return t === $ ? -1 : e === $ ? 1 : r ? -1 : s ? 1 : A ? J(A, t) - J(A, e) : 0;
                    if (r === s) return o(t, e);
                    for (i = t; i = i.parentNode;) a.unshift(i);
                    for (i = e; i = i.parentNode;) l.unshift(i);
                    for (; a[n] === l[n];) n++;
                    return n ? o(a[n], l[n]) : a[n] === F ? -1 : l[n] === F ? 1 : 0
                }, $) : $
            }, e.matches = function(t, i) {
                return e(t, null, null, i)
            }, e.matchesSelector = function(t, i) {
                if ((t.ownerDocument || t) !== $ && E(t), i = i.replace(lt, "='$1']"), w.matchesSelector && R && !X[i + " "] && (!N || !N.test(i)) && (!M || !M.test(i))) try {
                    var n = z.call(t, i);
                    if (n || w.disconnectedMatch || t.document && 11 !== t.document.nodeType) return n
                } catch (t) {}
                return e(i, $, null, [t]).length > 0
            }, e.contains = function(t, e) {
                return (t.ownerDocument || t) !== $ && E(t), j(t, e)
            }, e.attr = function(t, e) {
                (t.ownerDocument || t) !== $ && E(t);
                var i = x.attrHandle[e.toLowerCase()],
                    n = i && Y.call(x.attrHandle, e.toLowerCase()) ? i(t, e, !R) : void 0;
                return void 0 !== n ? n : w.attributes || !R ? t.getAttribute(e) : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
            }, e.escape = function(t) {
                return (t + "").replace(yt, wt)
            }, e.error = function(t) {
                throw new Error("Syntax error, unrecognized expression: " + t)
            }, e.uniqueSort = function(t) {
                var e, i = [],
                    n = 0,
                    r = 0;
                if (D = !w.detectDuplicates, A = !w.sortStable && t.slice(0), t.sort(W), D) {
                    for (; e = t[r++];) e === t[r] && (n = i.push(r));
                    for (; n--;) t.splice(i[n], 1)
                }
                return A = null, t
            }, b = e.getText = function(t) {
                var e, i = "",
                    n = 0,
                    r = t.nodeType;
                if (r) {
                    if (1 === r || 9 === r || 11 === r) {
                        if ("string" == typeof t.textContent) return t.textContent;
                        for (t = t.firstChild; t; t = t.nextSibling) i += b(t)
                    } else if (3 === r || 4 === r) return t.nodeValue
                } else
                    for (; e = t[n++];) i += b(e);
                return i
            }, (x = e.selectors = {
                cacheLength: 50,
                createPseudo: n,
                match: ut,
                attrHandle: {},
                find: {},
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: !0
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: !0
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    ATTR: function(t) {
                        return t[1] = t[1].replace(vt, _t), t[3] = (t[3] || t[4] || t[5] || "").replace(vt, _t), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
                    },
                    CHILD: function(t) {
                        return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]), t
                    },
                    PSEUDO: function(t) {
                        var e, i = !t[6] && t[2];
                        return ut.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : i && ht.test(i) && (e = C(i, !0)) && (e = i.indexOf(")", i.length - e) - i.length) && (t[0] = t[0].slice(0, e), t[2] = i.slice(0, e)), t.slice(0, 3))
                    }
                },
                filter: {
                    TAG: function(t) {
                        var e = t.replace(vt, _t).toLowerCase();
                        return "*" === t ? function() {
                            return !0
                        } : function(t) {
                            return t.nodeName && t.nodeName.toLowerCase() === e
                        }
                    },
                    CLASS: function(t) {
                        var e = H[t + " "];
                        return e || (e = new RegExp("(^|" + tt + ")" + t + "(" + tt + "|$)")) && H(t, function(t) {
                            return e.test("string" == typeof t.className && t.className || void 0 !== t.getAttribute && t.getAttribute("class") || "")
                        })
                    },
                    ATTR: function(t, i, n) {
                        return function(r) {
                            var s = e.attr(r, t);
                            return null == s ? "!=" === i : !i || (s += "", "=" === i ? s === n : "!=" === i ? s !== n : "^=" === i ? n && 0 === s.indexOf(n) : "*=" === i ? n && s.indexOf(n) > -1 : "$=" === i ? n && s.slice(-n.length) === n : "~=" === i ? (" " + s.replace(rt, " ") + " ").indexOf(n) > -1 : "|=" === i && (s === n || s.slice(0, n.length + 1) === n + "-"))
                        }
                    },
                    CHILD: function(t, e, i, n, r) {
                        var s = "nth" !== t.slice(0, 3),
                            o = "last" !== t.slice(-4),
                            a = "of-type" === e;
                        return 1 === n && 0 === r ? function(t) {
                            return !!t.parentNode
                        } : function(e, i, l) {
                            var h, c, u, p, d, f, m = s !== o ? "nextSibling" : "previousSibling",
                                g = e.parentNode,
                                v = a && e.nodeName.toLowerCase(),
                                _ = !l && !a,
                                y = !1;
                            if (g) {
                                if (s) {
                                    for (; m;) {
                                        for (p = e; p = p[m];)
                                            if (a ? p.nodeName.toLowerCase() === v : 1 === p.nodeType) return !1;
                                        f = m = "only" === t && !f && "nextSibling"
                                    }
                                    return !0
                                }
                                if (f = [o ? g.firstChild : g.lastChild], o && _) {
                                    for (y = (d = (h = (c = (u = (p = g)[L] || (p[L] = {}))[p.uniqueID] || (u[p.uniqueID] = {}))[t] || [])[0] === I && h[1]) && h[2], p = d && g.childNodes[d]; p = ++d && p && p[m] || (y = d = 0) || f.pop();)
                                        if (1 === p.nodeType && ++y && p === e) {
                                            c[t] = [I, d, y];
                                            break
                                        }
                                } else if (_ && (y = d = (h = (c = (u = (p = e)[L] || (p[L] = {}))[p.uniqueID] || (u[p.uniqueID] = {}))[t] || [])[0] === I && h[1]), !1 === y)
                                    for (;
                                        (p = ++d && p && p[m] || (y = d = 0) || f.pop()) && ((a ? p.nodeName.toLowerCase() !== v : 1 !== p.nodeType) || !++y || (_ && ((c = (u = p[L] || (p[L] = {}))[p.uniqueID] || (u[p.uniqueID] = {}))[t] = [I, y]), p !== e)););
                                return (y -= r) === n || y % n == 0 && y / n >= 0
                            }
                        }
                    },
                    PSEUDO: function(t, i) {
                        var r, s = x.pseudos[t] || x.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
                        return s[L] ? s(i) : s.length > 1 ? (r = [t, t, "", i], x.setFilters.hasOwnProperty(t.toLowerCase()) ? n(function(t, e) {
                            for (var n, r = s(t, i), o = r.length; o--;) t[n = J(t, r[o])] = !(e[n] = r[o])
                        }) : function(t) {
                            return s(t, 0, r)
                        }) : s
                    }
                },
                pseudos: {
                    not: n(function(t) {
                        var e = [],
                            i = [],
                            r = S(t.replace(st, "$1"));
                        return r[L] ? n(function(t, e, i, n) {
                            for (var s, o = r(t, null, n, []), a = t.length; a--;)(s = o[a]) && (t[a] = !(e[a] = s))
                        }) : function(t, n, s) {
                            return e[0] = t, r(e, null, s, i), e[0] = null, !i.pop()
                        }
                    }),
                    has: n(function(t) {
                        return function(i) {
                            return e(t, i).length > 0
                        }
                    }),
                    contains: n(function(t) {
                        return t = t.replace(vt, _t),
                            function(e) {
                                return (e.textContent || e.innerText || b(e)).indexOf(t) > -1
                            }
                    }),
                    lang: n(function(t) {
                        return ct.test(t || "") || e.error("unsupported lang: " + t), t = t.replace(vt, _t).toLowerCase(),
                            function(e) {
                                var i;
                                do {
                                    if (i = R ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (i = i.toLowerCase()) === t || 0 === i.indexOf(t + "-")
                                } while ((e = e.parentNode) && 1 === e.nodeType);
                                return !1
                            }
                    }),
                    target: function(e) {
                        var i = t.location && t.location.hash;
                        return i && i.slice(1) === e.id
                    },
                    root: function(t) {
                        return t === O
                    },
                    focus: function(t) {
                        return t === $.activeElement && (!$.hasFocus || $.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
                    },
                    enabled: a(!1),
                    disabled: a(!0),
                    checked: function(t) {
                        var e = t.nodeName.toLowerCase();
                        return "input" === e && !!t.checked || "option" === e && !!t.selected
                    },
                    selected: function(t) {
                        return t.parentNode && t.parentNode.selectedIndex, !0 === t.selected
                    },
                    empty: function(t) {
                        for (t = t.firstChild; t; t = t.nextSibling)
                            if (t.nodeType < 6) return !1;
                        return !0
                    },
                    parent: function(t) {
                        return !x.pseudos.empty(t)
                    },
                    header: function(t) {
                        return dt.test(t.nodeName)
                    },
                    input: function(t) {
                        return pt.test(t.nodeName)
                    },
                    button: function(t) {
                        var e = t.nodeName.toLowerCase();
                        return "input" === e && "button" === t.type || "button" === e
                    },
                    text: function(t) {
                        var e;
                        return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
                    },
                    first: l(function() {
                        return [0]
                    }),
                    last: l(function(t, e) {
                        return [e - 1]
                    }),
                    eq: l(function(t, e, i) {
                        return [i < 0 ? i + e : i]
                    }),
                    even: l(function(t, e) {
                        for (var i = 0; i < e; i += 2) t.push(i);
                        return t
                    }),
                    odd: l(function(t, e) {
                        for (var i = 1; i < e; i += 2) t.push(i);
                        return t
                    }),
                    lt: l(function(t, e, i) {
                        for (var n = i < 0 ? i + e : i; --n >= 0;) t.push(n);
                        return t
                    }),
                    gt: l(function(t, e, i) {
                        for (var n = i < 0 ? i + e : i; ++n < e;) t.push(n);
                        return t
                    })
                }
            }).pseudos.nth = x.pseudos.eq, {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) x.pseudos[y] = function(t) {
            return function(e) {
                return "input" === e.nodeName.toLowerCase() && e.type === t
            }
        }(y);
        for (y in {
                submit: !0,
                reset: !0
            }) x.pseudos[y] = function(t) {
            return function(e) {
                var i = e.nodeName.toLowerCase();
                return ("input" === i || "button" === i) && e.type === t
            }
        }(y);
        return c.prototype = x.filters = x.pseudos, x.setFilters = new c, C = e.tokenize = function(t, i) {
            var n, r, s, o, a, l, h, c = B[t + " "];
            if (c) return i ? 0 : c.slice(0);
            for (a = t, l = [], h = x.preFilter; a;) {
                for (o in n && !(r = ot.exec(a)) || (r && (a = a.slice(r[0].length) || a), l.push(s = [])), n = !1, (r = at.exec(a)) && (n = r.shift(), s.push({
                        value: n,
                        type: r[0].replace(st, " ")
                    }), a = a.slice(n.length)), x.filter) !(r = ut[o].exec(a)) || h[o] && !(r = h[o](r)) || (n = r.shift(), s.push({
                    value: n,
                    type: o,
                    matches: r
                }), a = a.slice(n.length));
                if (!n) break
            }
            return i ? a.length : a ? e.error(t) : B(t, l).slice(0)
        }, S = e.compile = function(t, e) {
            var i, n = [],
                r = [],
                s = X[t + " "];
            if (!s) {
                for (e || (e = C(t)), i = e.length; i--;)(s = v(e[i]))[L] ? n.push(s) : r.push(s);
                (s = X(t, _(r, n))).selector = t
            }
            return s
        }, P = e.select = function(t, e, i, n) {
            var r, s, o, a, l, c = "function" == typeof t && t,
                p = !n && C(t = c.selector || t);
            if (i = i || [], 1 === p.length) {
                if ((s = p[0] = p[0].slice(0)).length > 2 && "ID" === (o = s[0]).type && 9 === e.nodeType && R && x.relative[s[1].type]) {
                    if (!(e = (x.find.ID(o.matches[0].replace(vt, _t), e) || [])[0])) return i;
                    c && (e = e.parentNode), t = t.slice(s.shift().value.length)
                }
                for (r = ut.needsContext.test(t) ? 0 : s.length; r-- && (o = s[r], !x.relative[a = o.type]);)
                    if ((l = x.find[a]) && (n = l(o.matches[0].replace(vt, _t), gt.test(s[0].type) && h(e.parentNode) || e))) {
                        if (s.splice(r, 1), !(t = n.length && u(s))) return G.apply(i, n), i;
                        break
                    }
            }
            return (c || S(t, p))(n, e, !R, i, !e || gt.test(t) && h(e.parentNode) || e), i
        }, w.sortStable = L.split("").sort(W).join("") === L, w.detectDuplicates = !!D, E(), w.sortDetached = r(function(t) {
            return 1 & t.compareDocumentPosition($.createElement("fieldset"))
        }), r(function(t) {
            return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
        }) || s("type|href|height|width", function(t, e, i) {
            if (!i) return t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
        }), w.attributes && r(function(t) {
            return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
        }) || s("value", function(t, e, i) {
            if (!i && "input" === t.nodeName.toLowerCase()) return t.defaultValue
        }), r(function(t) {
            return null == t.getAttribute("disabled")
        }) || s(K, function(t, e, i) {
            var n;
            if (!i) return !0 === t[e] ? e.toLowerCase() : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
        }), e
    }(t);
    at.find = ht, at.expr = ht.selectors, at.expr[":"] = at.expr.pseudos, at.uniqueSort = at.unique = ht.uniqueSort, at.text = ht.getText, at.isXMLDoc = ht.isXML, at.contains = ht.contains, at.escapeSelector = ht.escape;
    var ct = function(t, e, i) {},
        ut = function(t, e) {
            for (var i = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && i.push(t);
            return i
        },
        pt = at.expr.match.needsContext,
        dt = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
        ft = /^.[^:#\[\.,]*$/;
    at.filter = function(t, e, i) {}, at.fn.extend({
        find: function(t) {
            var e, i, n = this.length,
                r = this;
            if ("string" != typeof t) return this.pushStack(at(t).filter(function() {
                for (e = 0; e < n; e++)
                    if (at.contains(r[e], this)) return !0
            }));
            for (i = this.pushStack([]), e = 0; e < n; e++) at.find(t, r[e], i);
            return n > 1 ? at.uniqueSort(i) : i
        },
        filter: function(t) {
            return this.pushStack(s(this, t || [], !1))
        },
        not: function(t) {
            return this.pushStack(s(this, t || [], !0))
        },
        is: function(t) {
            return !!s(this, "string" == typeof t && pt.test(t) ? at(t) : t || [], !1).length
        }
    });
    var mt, gt = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (at.fn.init = function(t, e, i) {
        var n, r;
        if (!t) return this;
        if (i = i || mt, "string" == typeof t) {
            if (!(n = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [null, t, null] : gt.exec(t)) || !n[1] && e) return !e || e.jquery ? (e || i).find(t) : this.constructor(e).find(t);
            if (n[1]) {
                if (e = e instanceof at ? e[0] : e, at.merge(this, at.parseHTML(n[1], e && e.nodeType ? e.ownerDocument || e : Q, !0)), dt.test(n[1]) && at.isPlainObject(e))
                    for (n in e) at.isFunction(this[n]) ? this[n](e[n]) : this.attr(n, e[n]);
                return this
            }
            return (r = Q.getElementById(n[2])) && (this[0] = r, this.length = 1), this
        }
        return t.nodeType ? (this[0] = t, this.length = 1, this) : at.isFunction(t) ? void 0 !== i.ready ? i.ready(t) : t(at) : at.makeArray(t, this)
    }).prototype = at.fn, mt = at(Q);
    var vt = /^(?:parents|prev(?:Until|All))/,
        _t = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    at.fn.extend({}), at.each({}, function(t, e) {});
    var yt = /[^\x20\t\r\n\f]+/g;
    at.Callbacks = function(t) {
        t = "string" == typeof t ? function(t) {
            var e = {};
            return at.each(t.match(yt) || [], function(t, i) {
                e[i] = !0
            }), e
        }(t) : at.extend({}, t);
        var e, i, n, r, s = [],
            o = [],
            a = -1,
            l = function() {
                for (r = r || t.once, n = e = !0; o.length; a = -1)
                    for (i = o.shift(); ++a < s.length;) !1 === s[a].apply(i[0], i[1]) && t.stopOnFalse && (a = s.length, i = !1);
                t.memory || (i = !1), e = !1, r && (s = i ? [] : "")
            },
            h = {
                add: function() {
                    return s && (i && !e && (a = s.length - 1, o.push(i)), function e(i) {
                        at.each(i, function(i, n) {
                            at.isFunction(n) ? t.unique && h.has(n) || s.push(n) : n && n.length && "string" !== at.type(n) && e(n)
                        })
                    }(arguments), i && !e && l()), this
                },
                remove: function() {
                    return at.each(arguments, function(t, e) {
                        for (var i;
                            (i = at.inArray(e, s, i)) > -1;) s.splice(i, 1), i <= a && a--
                    }), this
                },
                has: function(t) {
                    return t ? at.inArray(t, s) > -1 : s.length > 0
                },
                empty: function() {
                    return s && (s = []), this
                },
                disable: function() {
                    return r = o = [], s = i = "", this
                },
                disabled: function() {
                    return !s
                },
                lock: function() {
                    return r = o = [], i || e || (s = i = ""), this
                },
                locked: function() {
                    return !!r
                },
                fireWith: function(t, i) {
                    return r || (i = [t, (i = i || []).slice ? i.slice() : i], o.push(i), e || l()), this
                },
                fire: function() {
                    return h.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!n
                }
            };
        return h
    }, at.extend({
        Deferred: function(e) {
            var i = [
                    ["notify", "progress", at.Callbacks("memory"), at.Callbacks("memory"), 2],
                    ["resolve", "done", at.Callbacks("once memory"), at.Callbacks("once memory"), 0, "resolved"],
                    ["reject", "fail", at.Callbacks("once memory"), at.Callbacks("once memory"), 1, "rejected"]
                ],
                n = "pending",
                r = {
                    state: function() {
                        return n
                    },
                    always: function() {
                        return s.done(arguments).fail(arguments), this
                    },
                    catch: function(t) {
                        return r.then(null, t)
                    },
                    pipe: function() {
                        var t = arguments;
                        return at.Deferred(function(e) {
                            at.each(i, function(i, n) {
                                var r = at.isFunction(t[n[4]]) && t[n[4]];
                                s[n[1]](function() {
                                    var t = r && r.apply(this, arguments);
                                    t && at.isFunction(t.promise) ? t.promise().progress(e.notify).done(e.resolve).fail(e.reject) : e[n[0] + "With"](this, r ? [t] : arguments)
                                })
                            }), t = null
                        }).promise()
                    },
                    then: function(e, n, r) {
                        function s(e, i, n, r) {
                            return function() {
                                var h = this,
                                    c = arguments,
                                    u = function() {
                                        var t, u;
                                        if (!(e < o)) {
                                            if ((t = n.apply(h, c)) === i.promise()) throw new TypeError("Thenable self-resolution");
                                            u = t && ("object" == typeof t || "function" == typeof t) && t.then, at.isFunction(u) ? r ? u.call(t, s(o, i, a, r), s(o, i, l, r)) : (o++, u.call(t, s(o, i, a, r), s(o, i, l, r), s(o, i, a, i.notifyWith))) : (n !== a && (h = void 0, c = [t]), (r || i.resolveWith)(h, c))
                                        }
                                    },
                                    p = r ? u : function() {
                                        try {
                                            u()
                                        } catch (t) {
                                            at.Deferred.exceptionHook && at.Deferred.exceptionHook(t, p.stackTrace), e + 1 >= o && (n !== l && (h = void 0, c = [t]), i.rejectWith(h, c))
                                        }
                                    };
                                e ? p() : (at.Deferred.getStackHook && (p.stackTrace = at.Deferred.getStackHook()), t.setTimeout(p))
                            }
                        }
                        var o = 0;
                        return at.Deferred(function(t) {
                            i[0][3].add(s(0, t, at.isFunction(r) ? r : a, t.notifyWith)), i[1][3].add(s(0, t, at.isFunction(e) ? e : a)), i[2][3].add(s(0, t, at.isFunction(n) ? n : l))
                        }).promise()
                    },
                    promise: function(t) {
                        return null != t ? at.extend(t, r) : r
                    }
                },
                s = {};
            return at.each(i, function(t, e) {
                var o = e[2],
                    a = e[5];
                r[e[1]] = o.add, a && o.add(function() {
                    n = a
                }, i[3 - t][2].disable, i[0][2].lock), o.add(e[3].fire), s[e[0]] = function() {
                    return s[e[0] + "With"](this === s ? void 0 : this, arguments), this
                }, s[e[0] + "With"] = o.fireWith
            }), r.promise(s), e && e.call(s, s), s
        },
        when: function(t) {}
    });
    var wt = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    at.Deferred.exceptionHook = function(e, i) {}, at.readyException = function(e) {
        t.setTimeout(function() {
            throw e
        })
    };
    var xt = at.Deferred();
    at.fn.ready = function(t) {}, at.extend({
        isReady: !1,
        readyWait: 1,
        ready: function(t) {}
    }), at.ready.then = xt.then, "complete" === Q.readyState || "loading" !== Q.readyState && !Q.documentElement.doScroll ? t.setTimeout(at.ready) : (Q.addEventListener("DOMContentLoaded", c), t.addEventListener("load", c));
    var bt = function(t, e, i, n, r, s, o) {
            var a = 0,
                l = t.length,
                h = null == i;
            if ("object" === at.type(i))
                for (a in r = !0, i) bt(t, e, a, i[a], !0, s, o);
            else if (void 0 !== n && (r = !0, at.isFunction(n) || (o = !0), h && (o ? (e.call(t, n), e = null) : (h = e, e = function(t, e, i) {
                    return h.call(at(t), i)
                })), e))
                for (; a < l; a++) e(t[a], i, o ? n : n.call(t[a], a, e(t[a], i)));
            return r ? t : h ? e.call(t) : l ? e(t[0], i) : s
        },
        Tt = function(t) {
            return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType
        };
    u.uid = 1, u.prototype = {
        cache: function(t) {
            var e = t[this.expando];
            return e || (e = {}, Tt(t) && (t.nodeType ? t[this.expando] = e : Object.defineProperty(t, this.expando, {
                value: e,
                configurable: !0
            }))), e
        },
        set: function(t, e, i) {
            var n, r = this.cache(t);
            if ("string" == typeof e) r[at.camelCase(e)] = i;
            else
                for (n in e) r[at.camelCase(n)] = e[n];
            return r
        },
        get: function(t, e) {
            return void 0 === e ? this.cache(t) : t[this.expando] && t[this.expando][at.camelCase(e)]
        },
        access: function(t, e, i) {
            return void 0 === e || e && "string" == typeof e && void 0 === i ? this.get(t, e) : (this.set(t, e, i), void 0 !== i ? i : e)
        },
        remove: function(t, e) {
            var i, n = t[this.expando];
            if (void 0 !== n) {
                if (void 0 !== e) {
                    Array.isArray(e) ? e = e.map(at.camelCase) : e = (e = at.camelCase(e)) in n ? [e] : e.match(yt) || [], i = e.length;
                    for (; i--;) delete n[e[i]]
                }(void 0 === e || at.isEmptyObject(n)) && (t.nodeType ? t[this.expando] = void 0 : delete t[this.expando])
            }
        },
        hasData: function(t) {
            var e = t[this.expando];
            return void 0 !== e && !at.isEmptyObject(e)
        }
    };
    var Ct = new u,
        St = new u,
        Pt = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        kt = /[A-Z]/g;
    at.extend({
        hasData: function(t) {
            return St.hasData(t) || Ct.hasData(t)
        },
        data: function(t, e, i) {
            return St.access(t, e, i)
        },
        removeData: function(t, e) {
            St.remove(t, e)
        },
        _data: function(t, e, i) {
            return Ct.access(t, e, i)
        },
        _removeData: function(t, e) {
            Ct.remove(t, e)
        }
    }), at.fn.extend({}), at.extend({}), at.fn.extend({});
    var At = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        Dt = new RegExp("^(?:([+-])=|)(" + At + ")([a-z%]*)$", "i"),
        Et = ["Top", "Right", "Bottom", "Left"],
        $t = function(t, e) {},
        Ot = function(t, e, i, n) {},
        Rt = {};
    at.fn.extend({});
    var Mt = /^(?:checkbox|radio)$/i,
        Nt = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        zt = /^$|\/(?:java|ecma)script/i,
        jt = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    jt.optgroup = jt.option, jt.tbody = jt.tfoot = jt.colgroup = jt.caption = jt.thead, jt.th = jt.td;
    var Lt = /<|&#?\w+;/;
    ! function() {}();
    var Ft = Q.documentElement,
        It = /^key/,
        qt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        Ht = /^([^.]*)(?:\.(.+)|)/;
    at.event = {
        global: {},
        add: function(t, e, i, n, r) {
            var s, o, a, l, h, c, u, p, d, f, m, g = Ct.get(t);
            if (g)
                for (i.handler && (i = (s = i).handler, r = s.selector), r && at.find.matchesSelector(Ft, r), i.guid || (i.guid = at.guid++), (l = g.events) || (l = g.events = {}), (o = g.handle) || (o = g.handle = function(e) {
                        return void 0 !== at && at.event.triggered !== e.type ? at.event.dispatch.apply(t, arguments) : void 0
                    }), h = (e = (e || "").match(yt) || [""]).length; h--;) d = m = (a = Ht.exec(e[h]) || [])[1], f = (a[2] || "").split(".").sort(), d && (u = at.event.special[d] || {}, d = (r ? u.delegateType : u.bindType) || d, u = at.event.special[d] || {}, c = at.extend({
                    type: d,
                    origType: m,
                    data: n,
                    handler: i,
                    guid: i.guid,
                    selector: r,
                    needsContext: r && at.expr.match.needsContext.test(r),
                    namespace: f.join(".")
                }, s), (p = l[d]) || ((p = l[d] = []).delegateCount = 0, u.setup && !1 !== u.setup.call(t, n, f, o) || t.addEventListener && t.addEventListener(d, o)), u.add && (u.add.call(t, c), c.handler.guid || (c.handler.guid = i.guid)), r ? p.splice(p.delegateCount++, 0, c) : p.push(c), at.event.global[d] = !0)
        },
        remove: function(t, e, i, n, r) {},
        dispatch: function(t) {
            var e, i, n, r, s, o, a = at.event.fix(t),
                l = new Array(arguments.length),
                h = (Ct.get(this, "events") || {})[a.type] || [],
                c = at.event.special[a.type] || {};
            for (l[0] = a, e = 1; e < arguments.length; e++) l[e] = arguments[e];
            if (a.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, a)) {
                for (o = at.event.handlers.call(this, a, h), e = 0;
                    (r = o[e++]) && !a.isPropagationStopped();)
                    for (a.currentTarget = r.elem, i = 0;
                        (s = r.handlers[i++]) && !a.isImmediatePropagationStopped();) a.rnamespace && !a.rnamespace.test(s.namespace) || (a.handleObj = s, a.data = s.data, void 0 !== (n = ((at.event.special[s.origType] || {}).handle || s.handler).apply(r.elem, l)) && !1 === (a.result = n) && (a.preventDefault(), a.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, a), a.result
            }
        },
        handlers: function(t, e) {
            var i, n, r, s, o, a = [],
                l = e.delegateCount,
                h = t.target;
            if (l && h.nodeType && !("click" === t.type && t.button >= 1))
                for (; h !== this; h = h.parentNode || this)
                    if (1 === h.nodeType && ("click" !== t.type || !0 !== h.disabled)) {
                        for (s = [], o = {}, i = 0; i < l; i++) void 0 === o[r = (n = e[i]).selector + " "] && (o[r] = n.needsContext ? at(r, this).index(h) > -1 : at.find(r, this, null, [h]).length), o[r] && s.push(n);
                        s.length && a.push({
                            elem: h,
                            handlers: s
                        })
                    }
            return h = this, l < e.length && a.push({
                elem: h,
                handlers: e.slice(l)
            }), a
        },
        // from here.
        addProp: function(t, e) {},
        fix: function(t) {
            return t[at.expando] ? t : new at.Event(t)
        },
        special: {}
    }, at.removeEvent = function(t, e, i) {}, at.Event = function(t, e) {
        return this instanceof at.Event ? (t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && !1 === t.returnValue ? y : w, this.target = t.target && 3 === t.target.nodeType ? t.target.parentNode : t.target, this.currentTarget = t.currentTarget, this.relatedTarget = t.relatedTarget) : this.type = t, e && at.extend(this, e), this.timeStamp = t && t.timeStamp || at.now(), void(this[at.expando] = !0)) : new at.Event(t, e)
    }, at.Event.prototype = {
        constructor: at.Event,
        isDefaultPrevented: w,
        isPropagationStopped: w,
        isImmediatePropagationStopped: w,
        isSimulated: !1,
        preventDefault: function() {
            var t = this.originalEvent;
            this.isDefaultPrevented = y, t && !this.isSimulated && t.preventDefault()
        },
        stopPropagation: function() {
            var t = this.originalEvent;
            this.isPropagationStopped = y, t && !this.isSimulated && t.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var t = this.originalEvent;
            this.isImmediatePropagationStopped = y, t && !this.isSimulated && t.stopImmediatePropagation(), this.stopPropagation()
        }
    }, at.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(t) {
            var e = t.button;
            return null == t.which && It.test(t.type) ? null != t.charCode ? t.charCode : t.keyCode : !t.which && void 0 !== e && qt.test(t.type) ? 1 & e ? 1 : 2 & e ? 3 : 4 & e ? 2 : 0 : t.which
        }
    }, at.event.addProp), at.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(t, e) {
        at.event.special[t] = {
            delegateType: e,
            bindType: e,
            handle: function(t) {
                var i, n = t.relatedTarget,
                    r = t.handleObj;
                return n && (n === this || at.contains(this, n)) || (t.type = r.origType, i = r.handler.apply(this, arguments), t.type = e), i
            }
        }
    }), at.fn.extend({
        on: function(t, e, i, n) {
            return b(this, t, e, i, n)
        },
        one: function(t, e, i, n) {
            return b(this, t, e, i, n, 1)
        },
        off: function(t, e, i) {
            var n, r;
            if (t && t.preventDefault && t.handleObj) return n = t.handleObj, at(t.delegateTarget).off(n.namespace ? n.origType + "." + n.namespace : n.origType, n.selector, n.handler), this;
            if ("object" == typeof t) {
                for (r in t) this.off(r, e, t[r]);
                return this
            }
            return !1 !== e && "function" != typeof e || (i = e, e = void 0), !1 === i && (i = w), this.each(function() {
                at.event.remove(this, t, i, e)
            })
        }
    });
    var Bt = /<script|<style|<link/i,
        Xt = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Wt = /^true\/(.*)/,
        Yt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    at.extend({
        htmlPrefilter: function(t) {},
        clone: function(t, e, i) {},
        cleanData: function(t) {}
    }), at.fn.extend({
        detach: function(t) {
            return D(this, t, !0)
        },
        remove: function(t) {
            return D(this, t)
        },
        text: function(t) {
            return bt(this, function(t) {
                return void 0 === t ? at.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = t)
                })
            }, null, t, arguments.length)
        },
        append: function() {
            return A(this, arguments, function(t) {
                1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || T(this, t).appendChild(t)
            })
        },
        prepend: function() {
            return A(this, arguments, function(t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = T(this, t);
                    e.insertBefore(t, e.firstChild)
                }
            })
        },
        before: function() {
            return A(this, arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this)
            })
        },
        after: function() {
            return A(this, arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
            })
        },
        empty: function() {
            for (var t, e = 0; null != (t = this[e]); e++) 1 === t.nodeType && (at.cleanData(g(t, !1)), t.textContent = "");
            return this
        },
        clone: function(t, e) {
            return t = null != t && t, e = null == e ? t : e, this.map(function() {
                return at.clone(this, t, e)
            })
        },
        html: function(t) {
            return bt(this, function(t) {
                var e = this[0] || {},
                    i = 0,
                    n = this.length;
                if (void 0 === t && 1 === e.nodeType) return e.innerHTML;
                if ("string" == typeof t && !Bt.test(t) && !jt[(Nt.exec(t) || ["", ""])[1].toLowerCase()]) {
                    t = at.htmlPrefilter(t);
                    try {
                        for (; i < n; i++) 1 === (e = this[i] || {}).nodeType && (at.cleanData(g(e, !1)), e.innerHTML = t);
                        e = 0
                    } catch (t) {}
                }
                e && this.empty().append(t)
            }, null, t, arguments.length)
        },
        replaceWith: function() {
            var t = [];
            return A(this, arguments, function(e) {
                var i = this.parentNode;
                at.inArray(this, t) < 0 && (at.cleanData(g(this)), i && i.replaceChild(e, this))
            }, t)
        }
    }), at.each({}, function(t, e) {});
    var Ut = /^margin/,
        Vt = new RegExp("^(" + At + ")(?!px)[a-z%]+$", "i"),
        Qt = function(e) {
            var i = e.ownerDocument.defaultView;
            return i && i.opener || (i = t), i.getComputedStyle(e)
        };
    ! function() {}();
    var Gt = /^(none|table(?!-c[ea]).+)/,
        Zt = /^--/,
        Jt = {},
        Kt = {},
        te = ["Webkit", "Moz", "ms"],
        ee = Q.createElement("div").style;
    at.extend({
        cssHooks: {
            opacity: {
                get: function(t, e) {
                    if (e) {
                        var i = E(t, "opacity");
                        return "" === i ? "1" : i
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            float: "cssFloat"
        },
        style: function(t, e, i, n) {
            if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
                var r, s, o, a = at.camelCase(e),
                    l = Zt.test(e),
                    h = t.style;
                return l || (e = O(a)), o = at.cssHooks[e] || at.cssHooks[a], void 0 === i ? o && "get" in o && void 0 !== (r = o.get(t, !1, n)) ? r : h[e] : ("string" === (s = typeof i) && (r = Dt.exec(i)) && r[1] && (i = d(t, e, r), s = "number"), void(null != i && i == i && ("number" === s && (i += r && r[3] || (at.cssNumber[a] ? "" : "px")), ot.clearCloneStyle || "" !== i || 0 !== e.indexOf("background") || (h[e] = "inherit"), o && "set" in o && void 0 === (i = o.set(t, i, n)) || (l ? h.setProperty(e, i) : h[e] = i))))
            }
        },
        css: function(t, e, i, n) {
            var r, s, o, a = at.camelCase(e);
            return Zt.test(e) || (e = O(a)), (o = at.cssHooks[e] || at.cssHooks[a]) && "get" in o && (r = o.get(t, !0, i)), void 0 === r && (r = E(t, e, n)), "normal" === r && e in Kt && (r = Kt[e]), "" === i || i ? (s = parseFloat(r), !0 === i || isFinite(s) ? s || 0 : r) : r
        }
    }), at.each(["height", "width"], function(t, e) {}), at.cssHooks.marginLeft = $(ot.reliableMarginLeft, function(t, e) {
        if (e) return (parseFloat(E(t, "marginLeft")) || t.getBoundingClientRect().left - Ot(t, {}, function() {
            return t.getBoundingClientRect().left
        })) + "px"
    }), at.each({}, function(t, e) {}), at.fn.extend({
        css: function(t, e) {
            return bt(this, function(t, e, i) {
                var n, r, s = {},
                    o = 0;
                if (Array.isArray(e)) {
                    for (n = Qt(t), r = e.length; o < r; o++) s[e[o]] = at.css(t, e[o], !1, n);
                    return s
                }
                return void 0 !== i ? at.style(t, e, i) : at.css(t, e)
            }, t, e, arguments.length > 1)
        }
    }), at.Tween = z, z.prototype = {
        constructor: z,
        init: function(t, e, i, n, r, s) {
            this.elem = t, this.prop = i, this.easing = r || at.easing._default, this.options = e, this.start = this.now = this.cur(), this.end = n, this.unit = s || (at.cssNumber[i] ? "" : "px")
        },
        cur: function() {
            var t = z.propHooks[this.prop];
            return t && t.get ? t.get(this) : z.propHooks._default.get(this)
        },
        run: function(t) {
            var e, i = z.propHooks[this.prop];
            return this.options.duration ? this.pos = e = at.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), i && i.set ? i.set(this) : z.propHooks._default.set(this), this
        }
    }, z.prototype.init.prototype = z.prototype, z.propHooks = {}, z.propHooks.scrollTop = z.propHooks.scrollLeft = {
        set: function(t) {
            t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
        }
    }, at.easing = {}, at.fx = z.prototype.init, at.fx.step = {};
    var ie, ne, re = /^(?:toggle|show|hide)$/,
        se = /queueHooks$/;
    at.Animation = at.extend(q, {}), at.speed = function(t, e, i) {}, at.fn.extend({}), at.each(["toggle", "show", "hide"], function(t, e) {
            var i = at.fn[e];
            at.fn[e] = function(t, n, r) {}
        }), at.each({}, function(t, e) {}), at.timers = [], at.fx.tick = function() {}, at.fx.timer = function(t) {
            at.timers.push(t), at.fx.start()
        }, at.fx.interval = 13, at.fx.start = function() {}, at.fx.stop = function() {}, at.fx.speeds = {}, at.fn.delay = function(e, i) {
            return e = at.fx && at.fx.speeds[e] || e, i = i || "fx", this.queue(i, function(i, n) {})
        },
        function() {}();
    var oe, ae = at.expr.attrHandle;
    at.fn.extend({
        attr: function(t, e) {
            return bt(this, at.attr, t, e, arguments.length > 1)
        },
        removeAttr: function(t) {
            return this.each(function() {
                at.removeAttr(this, t)
            })
        }
    }), at.extend({
        attr: function(t, e, i) {
            var n, r, s = t.nodeType;
            if (3 !== s && 8 !== s && 2 !== s) return void 0 === t.getAttribute ? at.prop(t, e, i) : (1 === s && at.isXMLDoc(t) || (r = at.attrHooks[e.toLowerCase()] || (at.expr.match.bool.test(e) ? oe : void 0)), void 0 !== i ? null === i ? void at.removeAttr(t, e) : r && "set" in r && void 0 !== (n = r.set(t, i, e)) ? n : (t.setAttribute(e, i + ""), i) : r && "get" in r && null !== (n = r.get(t, e)) ? n : null == (n = at.find.attr(t, e)) ? void 0 : n)
        },
        attrHooks: {
            type: {
                set: function(t, e) {
                    if (!ot.radioValue && "radio" === e && r(t, "input")) {
                        var i = t.value;
                        return t.setAttribute("type", e), i && (t.value = i), e
                    }
                }
            }
        },
        removeAttr: function(t, e) {
            var i, n = 0,
                r = e && e.match(yt);
            if (r && 1 === t.nodeType)
                for (; i = r[n++];) t.removeAttribute(i)
        }
    }), oe = {}, at.each(at.expr.match.bool.source.match(/\w+/g), function(t, e) {});
    var le = /^(?:input|select|textarea|button)$/i,
        he = /^(?:a|area)$/i;
    at.fn.extend({}), at.extend({
        prop: function(t, e, i) {
            var n, r, s = t.nodeType;
            if (3 !== s && 8 !== s && 2 !== s) return 1 === s && at.isXMLDoc(t) || (e = at.propFix[e] || e, r = at.propHooks[e]), void 0 !== i ? r && "set" in r && void 0 !== (n = r.set(t, i, e)) ? n : t[e] = i : r && "get" in r && null !== (n = r.get(t, e)) ? n : t[e]
        },
        propHooks: {
            tabIndex: {
                get: function(t) {
                    var e = at.find.attr(t, "tabindex");
                    return e ? parseInt(e, 10) : le.test(t.nodeName) || he.test(t.nodeName) && t.href ? 0 : -1
                }
            }
        },
        propFix: {
            for: "htmlFor",
            class: "className"
        }
    }), ot.optSelected || (at.propHooks.selected = {}), at.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        at.propFix[this.toLowerCase()] = this
    }), at.fn.extend({
        addClass: function(t) {
            var e, i, n, r, s, o, a, l = 0;
            if (at.isFunction(t)) return this.each(function(e) {
                at(this).addClass(t.call(this, e, B(this)))
            });
            if ("string" == typeof t && t)
                for (e = t.match(yt) || []; i = this[l++];)
                    if (r = B(i), n = 1 === i.nodeType && " " + H(r) + " ") {
                        for (o = 0; s = e[o++];) n.indexOf(" " + s + " ") < 0 && (n += s + " ");
                        r !== (a = H(n)) && i.setAttribute("class", a)
                    }
            return this
        },
        removeClass: function(t) {
            var e, i, n, r, s, o, a, l = 0;
            if (at.isFunction(t)) return this.each(function(e) {
                at(this).removeClass(t.call(this, e, B(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof t && t)
                for (e = t.match(yt) || []; i = this[l++];)
                    if (r = B(i), n = 1 === i.nodeType && " " + H(r) + " ") {
                        for (o = 0; s = e[o++];)
                            for (; n.indexOf(" " + s + " ") > -1;) n = n.replace(" " + s + " ", " ");
                        r !== (a = H(n)) && i.setAttribute("class", a)
                    }
            return this
        },

        toggleClass: function(t, e) {},
        hasClass: function(t) {}
    }), at.fn.extend({
        val: function(t) {}
    }), at.extend({
        valHooks: {
            option: {
                get: function(t) {
                    var e = at.find.attr(t, "value");
                    return null != e ? e : H(at.text(t))
                }
            },
            select: {
                get: function(t) {
                    var e, i, n, s = t.options,
                        o = t.selectedIndex,
                        a = "select-one" === t.type,
                        l = a ? null : [],
                        h = a ? o + 1 : s.length;
                    for (n = o < 0 ? h : a ? o : 0; n < h; n++)
                        if (((i = s[n]).selected || n === o) && !i.disabled && (!i.parentNode.disabled || !r(i.parentNode, "optgroup"))) {
                            if (e = at(i).val(), a) return e;
                            l.push(e)
                        }
                    return l
                },
                set: function(t, e) {
                    for (var i, n, r = t.options, s = at.makeArray(e), o = r.length; o--;)((n = r[o]).selected = at.inArray(at.valHooks.option.get(n), s) > -1) && (i = !0);
                    return i || (t.selectedIndex = -1), s
                }
            }
        }
    }), at.each(["radio", "checkbox"], function() {});
    var ce = /^(?:focusinfocus|focusoutblur)$/;
    at.extend(at.event, {}), at.fn.extend({
        trigger: function(t, e) {},
        triggerHandler: function(t, e) {}
    }), at.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(t, e) {
        at.fn[e] = function(t, i) {
            return arguments.length > 0 ? this.on(e, null, t, i) : this.trigger(e)
        }
    }), at.fn.extend({}), ot.focusin = "onfocusin" in t, ot.focusin || at.each({}, function(t, e) {
        var i = function(t) {
            at.event.simulate(e, t.target, at.event.fix(t))
        };
        at.event.special[e] = {}
    });
    var ue = t.location,
        pe = at.now(),
        de = /\?/;
    at.parseXML = function(e) {};
    var fe = /\[\]$/,
        me = /^(?:submit|button|image|reset|file)$/i,
        ge = /^(?:input|select|textarea|keygen)/i;
    at.param = function(t, e) {}, at.fn.extend({
        serialize: function() {},
        serializeArray: function() {}
    });
    var ve = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        _e = /^(?:GET|HEAD)$/,
        ye = {},
        we = {},
        xe = "*/".concat("*"),
        be = Q.createElement("a");
    be.href = ue.href, at.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: ue.href,
            type: "GET",
            isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(ue.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": xe,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": at.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(t, e) {
            return e ? U(U(t, at.ajaxSettings), e) : U(at.ajaxSettings, t)
        },
        ajaxPrefilter: W(ye),
        ajaxTransport: W(we),
        ajax: function(e, i) {
            function n(e, i, n, a) {
                var h, p, d, w, x, b = i;
                c || (c = !0, l && t.clearTimeout(l), r = void 0, o = a || "", T.readyState = e > 0 ? 4 : 0, h = e >= 200 && e < 300 || 304 === e, n && (w = function(t, e, i) {
                    for (var n, r, s, o, a = t.contents, l = t.dataTypes;
                        "*" === l[0];) l.shift(), void 0 === n && (n = t.mimeType || e.getResponseHeader("Content-Type"));
                    if (n)
                        for (r in a)
                            if (a[r] && a[r].test(n)) {
                                l.unshift(r);
                                break
                            }
                    if (l[0] in i) s = l[0];
                    else {
                        for (r in i) {
                            if (!l[0] || t.converters[r + " " + l[0]]) {
                                s = r;
                                break
                            }
                            o || (o = r)
                        }
                        s = s || o
                    }
                    if (s) return s !== l[0] && l.unshift(s), i[s]
                }(f, T, n)), w = function(t, e, i, n) {
                    var r, s, o, a, l, h = {},
                        c = t.dataTypes.slice();
                    if (c[1])
                        for (o in t.converters) h[o.toLowerCase()] = t.converters[o];
                    for (s = c.shift(); s;)
                        if (t.responseFields[s] && (i[t.responseFields[s]] = e), !l && n && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = s, s = c.shift())
                            if ("*" === s) s = l;
                            else if ("*" !== l && l !== s) {
                        if (!(o = h[l + " " + s] || h["* " + s]))
                            for (r in h)
                                if ((a = r.split(" "))[1] === s && (o = h[l + " " + a[0]] || h["* " + a[0]])) {
                                    !0 === o ? o = h[r] : !0 !== h[r] && (s = a[0], c.unshift(a[1]));
                                    break
                                }
                        if (!0 !== o)
                            if (o && t.throws) e = o(e);
                            else try {
                                e = o(e)
                            } catch (t) {
                                return {
                                    state: "parsererror",
                                    error: o ? t : "No conversion from " + l + " to " + s
                                }
                            }
                    }
                    return {
                        state: "success",
                        data: e
                    }
                }(f, w, T, h), h ? (f.ifModified && ((x = T.getResponseHeader("Last-Modified")) && (at.lastModified[s] = x), (x = T.getResponseHeader("etag")) && (at.etag[s] = x)), 204 === e || "HEAD" === f.type ? b = "nocontent" : 304 === e ? b = "notmodified" : (b = w.state, p = w.data, h = !(d = w.error))) : (d = b, !e && b || (b = "error", e < 0 && (e = 0))), T.status = e, T.statusText = (i || b) + "", h ? v.resolveWith(m, [p, b, T]) : v.rejectWith(m, [T, b, d]), T.statusCode(y), y = void 0, u && g.trigger(h ? "ajaxSuccess" : "ajaxError", [T, f, h ? p : d]), _.fireWith(m, [T, b]), u && (g.trigger("ajaxComplete", [T, f]), --at.active || at.event.trigger("ajaxStop")))
            }
            "object" == typeof e && (i = e, e = void 0), i = i || {};
            var r, s, o, a, l, h, c, u, p, d, f = at.ajaxSetup({}, i),
                m = f.context || f,
                g = f.context && (m.nodeType || m.jquery) ? at(m) : at.event,
                v = at.Deferred(),
                _ = at.Callbacks("once memory"),
                y = f.statusCode || {},
                w = {},
                x = {},
                b = "canceled",
                T = {
                    readyState: 0,
                    getResponseHeader: function(t) {
                        var e;
                        if (c) {
                            if (!a)
                                for (a = {}; e = ve.exec(o);) a[e[1].toLowerCase()] = e[2];
                            e = a[t.toLowerCase()]
                        }
                        return null == e ? null : e
                    },
                    getAllResponseHeaders: function() {
                        return c ? o : null
                    },
                    setRequestHeader: function(t, e) {
                        return null == c && (t = x[t.toLowerCase()] = x[t.toLowerCase()] || t, w[t] = e), this
                    },
                    overrideMimeType: function(t) {
                        return null == c && (f.mimeType = t), this
                    },
                    statusCode: function(t) {
                        var e;
                        if (t)
                            if (c) T.always(t[T.status]);
                            else
                                for (e in t) y[e] = [y[e], t[e]];
                        return this
                    },
                    abort: function(t) {
                        var e = t || b;
                        return r && r.abort(e), n(0, e), this
                    }
                };
            if (v.promise(T), f.url = ((e || f.url || ue.href) + "").replace(/^\/\//, ue.protocol + "//"), f.type = i.method || i.type || f.method || f.type, f.dataTypes = (f.dataType || "*").toLowerCase().match(yt) || [""], null == f.crossDomain) {
                h = Q.createElement("a");
                try {
                    h.href = f.url, h.href = h.href, f.crossDomain = be.protocol + "//" + be.host != h.protocol + "//" + h.host
                } catch (t) {
                    f.crossDomain = !0
                }
            }
            if (f.data && f.processData && "string" != typeof f.data && (f.data = at.param(f.data, f.traditional)), Y(ye, f, i, T), c) return T;
            for (p in (u = at.event && f.global) && 0 == at.active++ && at.event.trigger("ajaxStart"), f.type = f.type.toUpperCase(), f.hasContent = !_e.test(f.type), s = f.url.replace(/#.*$/, ""), f.hasContent ? f.data && f.processData && 0 === (f.contentType || "").indexOf("application/x-www-form-urlencoded") && (f.data = f.data.replace(/%20/g, "+")) : (d = f.url.slice(s.length), f.data && (s += (de.test(s) ? "&" : "?") + f.data, delete f.data), !1 === f.cache && (s = s.replace(/([?&])_=[^&]*/, "$1"), d = (de.test(s) ? "&" : "?") + "_=" + pe++ + d), f.url = s + d), f.ifModified && (at.lastModified[s] && T.setRequestHeader("If-Modified-Since", at.lastModified[s]), at.etag[s] && T.setRequestHeader("If-None-Match", at.etag[s])), (f.data && f.hasContent && !1 !== f.contentType || i.contentType) && T.setRequestHeader("Content-Type", f.contentType), T.setRequestHeader("Accept", f.dataTypes[0] && f.accepts[f.dataTypes[0]] ? f.accepts[f.dataTypes[0]] + ("*" !== f.dataTypes[0] ? ", " + xe + "; q=0.01" : "") : f.accepts["*"]), f.headers) T.setRequestHeader(p, f.headers[p]);
            if (f.beforeSend && (!1 === f.beforeSend.call(m, T, f) || c)) return T.abort();
            if (b = "abort", _.add(f.complete), T.done(f.success), T.fail(f.error), r = Y(we, f, i, T)) {
                if (T.readyState = 1, u && g.trigger("ajaxSend", [T, f]), c) return T;
                f.async && f.timeout > 0 && (l = t.setTimeout(function() {
                    T.abort("timeout")
                }, f.timeout));
                try {
                    c = !1, r.send(w, n)
                } catch (t) {
                    if (c) throw t;
                    n(-1, t)
                }
            } else n(-1, "No Transport");
            return T
        },
        getJSON: function(t, e, i) {
            return at.get(t, e, i, "json")
        },
        getScript: function(t, e) {
            return at.get(t, void 0, e, "script")
        }
    }), at.each(["get", "post"], function(t, e) {}), at._evalUrl = function(t) {}, at.fn.extend({}), at.expr.pseudos.hidden = function(t) {
        return !at.expr.pseudos.visible(t)
    }, at.expr.pseudos.visible = function(t) {}, at.ajaxSettings.xhr = function() {};
    var Te = {
            0: 200,
            1223: 204
        },
        Ce = at.ajaxSettings.xhr();
    ot.cors = !!Ce && "withCredentials" in Ce, ot.ajax = Ce = !!Ce, at.ajaxTransport(function(e) {}), at.ajaxPrefilter(function(t) {
        t.crossDomain && (t.contents.script = !1)
    }), at.ajaxSetup({}), at.ajaxPrefilter("script", function(t) {}), at.ajaxTransport("script", function(t) {});
    var Se = [],
        Pe = /(=)\?(?=&|$)|\?\?/;
    at.ajaxSetup({}), at.ajaxPrefilter("json jsonp", function(e, i, n) {
            var r, s, o, a = !1 !== e.jsonp && (Pe.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Pe.test(e.data) && "data");
            if (a || "jsonp" === e.dataTypes[0]) return r = e.jsonpCallback = at.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(Pe, "$1" + r) : !1 !== e.jsonp && (e.url += (de.test(e.url) ? "&" : "?") + e.jsonp + "=" + r), e.converters["script json"] = function() {
                return o || at.error(r + " was not called"), o[0]
            }, e.dataTypes[0] = "json", s = t[r], t[r] = function() {
                o = arguments
            }, n.always(function() {}), "script"
        }), ot.createHTMLDocument = function() {}(), at.parseHTML = function(t, e, i) {
            return "string" != typeof t ? [] : ("boolean" == typeof e && (i = e, e = !1), e || (ot.createHTMLDocument ? ((n = (e = Q.implementation.createHTMLDocument("")).createElement("base")).href = Q.location.href, e.head.appendChild(n)) : e = Q), s = !i && [], (r = dt.exec(t)) ? [e.createElement(r[1])] : (r = _([t], e, s), s && s.length && at(s).remove(), at.merge([], r.childNodes)));
            var n, r, s
        }, at.fn.load = function(t, e, i) {}, at.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(t, e) {
            at.fn[e] = function(t) {
                return this.on(e, t)
            }
        }), at.expr.pseudos.animated = function(t) {}, at.offset = {},
        //from here 
        at.fn.extend({
            offset: function(t) {
                if (arguments.length) return void 0 === t ? this : this.each(function(e) {
                    at.offset.setOffset(this, t, e)
                });
                var e, i, n, r, s = this[0];
                return s ? s.getClientRects().length ? (n = s.getBoundingClientRect(), i = (e = s.ownerDocument).documentElement, r = e.defaultView, {
                    top: n.top + r.pageYOffset - i.clientTop,
                    left: n.left + r.pageXOffset - i.clientLeft
                }) : {
                    top: 0,
                    left: 0
                } : void 0
            },
            position: function() {
                if (this[0]) {
                    var t, e, i = this[0],
                        n = {
                            top: 0,
                            left: 0
                        };
                    return "fixed" === at.css(i, "position") ? e = i.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), r(t[0], "html") || (n = t.offset()), n = {
                        top: n.top + at.css(t[0], "borderTopWidth", !0),
                        left: n.left + at.css(t[0], "borderLeftWidth", !0)
                    }), {
                        top: e.top - n.top - at.css(i, "marginTop", !0),
                        left: e.left - n.left - at.css(i, "marginLeft", !0)
                    }
                }
            },
            offsetParent: function() {
                return this.map(function() {
                    for (var t = this.offsetParent; t && "static" === at.css(t, "position");) t = t.offsetParent;
                    return t || Ft
                })
            }
        }), at.each({}, function(t, e) {}), at.each(["top", "left"], function(t, e) {}), at.each({
            Height: "height",
            Width: "width"
        }, function(t, e) {
            at.each({
                padding: "inner" + t,
                content: e,
                "": "outer" + t
            }, function(i, n) {
                at.fn[n] = function(r, s) {
                    var o = arguments.length && (i || "boolean" != typeof r),
                        a = i || (!0 === r || !0 === s ? "margin" : "border");
                    return bt(this, function(e, i, r) {
                        var s;
                        return at.isWindow(e) ? 0 === n.indexOf("outer") ? e["inner" + t] : e.document.documentElement["client" + t] : 9 === e.nodeType ? (s = e.documentElement, Math.max(e.body["scroll" + t], s["scroll" + t], e.body["offset" + t], s["offset" + t], s["client" + t])) : void 0 === r ? at.css(e, i, a) : at.style(e, i, r, a)
                    }, e, o ? r : void 0, o)
                }
            })
        }), at.fn.extend({}), at.holdReady = function(t) {}, at.isArray = Array.isArray, at.parseJSON = JSON.parse, at.nodeName = r, "function" == typeof define && define.amd && define("jquery", [], function() {
            return at
        });
    var ke = t.jQuery,
        Ae = t.$;
    return at.noConflict = function(e) {}, e || (t.jQuery = t.$ = at), at
}),
function(t, e) {
    "function" == typeof define && define.amd ? define(e) : "object" == typeof exports ? module.exports = e() : t.ScrollMagic = e()
}(this, function() {
    "use strict";
    var t = function() {};
    t.version = "2.0.5", window.addEventListener("mousewheel", function() {});
    var e = "data-scrollmagic-pin-spacer";
    t.Controller = function(n) {};
    //from here
    var i = {};
    t.Controller.addOption = function(t, e) {}, t.Controller.extend = function(e) {}, t.Scene = function(i) {};
    var n = {
        defaults: {
            duration: 0,
            offset: 0,
            triggerElement: void 0,
            triggerHook: .5,
            reverse: !0,
            loglevel: 2
        },
        validate: {
            offset: function(t) {
                if (t = parseFloat(t), !r.type.Number(t)) throw 0;
                return t
            },
            triggerElement: function(t) {
                if (t = t || void 0) {
                    var e = r.get.elements(t)[0];
                    if (!e) throw 0;
                    t = e
                }
                return t
            },
            triggerHook: function(t) {
                var e = {
                    onCenter: .5,
                    onEnter: 1,
                    onLeave: 0
                };
                if (r.type.Number(t)) t = Math.max(0, Math.min(parseFloat(t), 1));
                else {
                    if (!(t in e)) throw 0;
                    t = e[t]
                }
                return t
            },
            reverse: function(t) {
                return !!t
            }
        },
        shifts: ["duration", "offset", "triggerHook"]
    };
    t.Scene.addOption = function(t, e, i, r) {}, t.Scene.extend = function(e) {}, t.Event = function(t, e, i, n) {};
    var r = t._util = function(t) {}(window || {});
    return t
});
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
if ((_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function() {}), _gsScope._gsDefine && _gsScope._gsQueue.pop()(), function(t, e) {}("undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window), function(t, e) {
        "function" == typeof define && define.amd ? define(["ScrollMagic", "TweenMax", "TimelineMax"], e) : "object" == typeof exports ? (require("gsap"), e(require("scrollmagic"), TweenMax, TimelineMax)) : e(t.ScrollMagic || t.jQuery && t.jQuery.ScrollMagic, t.TweenMax || t.TweenLite, t.TimelineMax || t.TimelineLite)
    }(this, function(t, e, i) {
        var n = window.console || {},
            r = Function.prototype.bind.call(n.error || n.log || function() {}, n);
        t || r("(animation.gsap)Â ->Â ERROR:Â TheÂ ScrollMagicÂ mainÂ moduleÂ couldÂ notÂ beÂ found.Â PleaseÂ makeÂ sureÂ it'sÂ loadedÂ beforeÂ thisÂ pluginÂ orÂ useÂ anÂ asynchronousÂ loaderÂ likeÂ requirejs."), e || r("(animation.gsap)Â ->Â ERROR:Â TweenLiteÂ orÂ TweenMaxÂ couldÂ notÂ beÂ found.Â PleaseÂ makeÂ sureÂ GSAPÂ isÂ loadedÂ beforeÂ ScrollMagicÂ orÂ useÂ anÂ asynchronousÂ loaderÂ likeÂ requirejs."), t.Scene.addOption("tweenChanges", !1, function(t) {}), t.Scene.extend(function() {
            var t, n = this,
                r = function() {};
            n.on("progress.plugin_gsap", function() {}), n.on("destroy.plugin_gsap", function(t) {});
            var s = function() {};
            n.setTween = function(o, a, l) {}, n.removeTween = function(e) {}
        })
    }), function(t) {}
    (function(t) {}), !window.YT) var YT = {};
if (!window.YTConfig) var YTConfig = {};
YT.loading || (YT.loading = 1, function() {}());
var players = [],
    $body = $("body");
if (!$body.hasClass("mobile")) {}
var options = {},
    docs = document.getElementsByClassName("countup"),
    countupsArray = [],
    controller = new ScrollMagic.Controller,
    staticController = new ScrollMagic.Controller,
    mainAnimation = function() {};
$(window).on("load", function() {}), $(window).on("resize", function() {});
var enableCountup = function(t) {},
    disableCountup = function(t) {};
! function(t, e, i, n) {
    function r(e, i) {
        this.settings = null, this.options = t.extend({}, r.Defaults, i), this.$element = t(e), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
            time: null,
            target: null,
            pointer: null,
            stage: {
                start: null,
                current: null
            },
            direction: null
        }, this._states = {
            current: {},
            tags: {
                initializing: ["busy"],
                animating: ["busy"],
                dragging: ["interacting"]
            }
        }, t.each(["onResize", "onThrottledResize"], t.proxy(function(e, i) {
            this._handlers[i] = t.proxy(this[i], this)
        }, this)), t.each(r.Plugins, t.proxy(function(t, e) {
            this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this)
        }, this)), t.each(r.Workers, t.proxy(function(e, i) {
            this._pipe.push({
                filter: i.filter,
                run: t.proxy(i.run, this)
            })
        }, this)), this.setup(), this.initialize()
    }
    r.Defaults = {
        items: 3,
        loop: !1,
        center: !1,
        rewind: !1,
        mouseDrag: !0,
        touchDrag: !0,
        pullDrag: !0,
        freeDrag: !1,
        margin: 0,
        stagePadding: 0,
        merge: !1,
        mergeFit: !0,
        autoWidth: !1,
        startPosition: 0,
        rtl: !1,
        smartSpeed: 250,
        fluidSpeed: !1,
        dragEndSpeed: !1,
        responsive: {},
        responsiveRefreshRate: 200,
        responsiveBaseElement: e,
        fallbackEasing: "swing",
        info: !1,
        nestedItemSelector: !1,
        itemElement: "div",
        stageElement: "div",
        refreshClass: "owl-refresh",
        loadedClass: "owl-loaded",
        loadingClass: "owl-loading",
        rtlClass: "owl-rtl",
        responsiveClass: "owl-responsive",
        dragClass: "owl-drag",
        itemClass: "owl-item",
        stageClass: "owl-stage",
        stageOuterClass: "owl-stage-outer",
        grabClass: "owl-grab"
    }, r.Width = {
        Default: "default",
        Inner: "inner",
        Outer: "outer"
    }, r.Type = {
        Event: "event",
        State: "state"
    }, r.Plugins = {}, r.Workers = [{
        filter: ["width", "settings"],
        run: function() {
            this._width = this.$element.width()
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            t.current = this._items && this._items[this.relative(this._current)]
        }
    }, {
        filter: ["items", "settings"],
        run: function() {
            this.$stage.children(".cloned").remove()
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            var e = this.settings.margin || "",
                i = !this.settings.autoWidth,
                n = this.settings.rtl,
                r = {
                    width: "auto",
                    "margin-left": n ? e : "",
                    "margin-right": n ? "" : e
                };
            !i && this.$stage.children().css(r), t.css = r
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            var e = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
                i = null,
                n = this._items.length,
                r = !this.settings.autoWidth,
                s = [];
            for (t.items = {
                    merge: !1,
                    width: e
                }; n--;) i = this._mergers[n], i = this.settings.mergeFit && Math.min(i, this.settings.items) || i, t.items.merge = i > 1 || t.items.merge, s[n] = r ? e * i : this._items[n].width();
            this._widths = s
        }
    }, {
        filter: ["items", "settings"],
        run: function() {
            var e = [],
                i = this._items,
                n = this.settings,
                r = Math.max(2 * n.items, 4),
                s = 2 * Math.ceil(i.length / 2),
                o = n.loop && i.length ? n.rewind ? r : Math.max(r, s) : 0,
                a = "",
                l = "";
            for (o /= 2; o--;) e.push(this.normalize(e.length / 2, !0)), a += i[e[e.length - 1]][0].outerHTML, e.push(this.normalize(i.length - 1 - (e.length - 1) / 2, !0)), l = i[e[e.length - 1]][0].outerHTML + l;
            this._clones = e, t(a).addClass("cloned").appendTo(this.$stage), t(l).addClass("cloned").prependTo(this.$stage)
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function() {
            for (var t = this.settings.rtl ? 1 : -1, e = this._clones.length + this._items.length, i = -1, n = 0, r = 0, s = []; ++i < e;) n = s[i - 1] || 0, r = this._widths[this.relative(i)] + this.settings.margin, s.push(n + r * t);
            this._coordinates = s
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function() {
            var t = this.settings.stagePadding,
                e = this._coordinates,
                i = {
                    width: Math.ceil(Math.abs(e[e.length - 1])) + 2 * t,
                    "padding-left": t || "",
                    "padding-right": t || ""
                };
            this.$stage.css(i)
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            var e = this._coordinates.length,
                i = !this.settings.autoWidth,
                n = this.$stage.children();
            if (i && t.items.merge)
                for (; e--;) t.css.width = this._widths[this.relative(e)], n.eq(e).css(t.css);
            else i && (t.css.width = t.items.width, n.css(t.css))
        }
    }, {
        filter: ["items"],
        run: function() {
            this._coordinates.length < 1 && this.$stage.removeAttr("style")
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            t.current = t.current ? this.$stage.children().index(t.current) : 0, t.current = Math.max(this.minimum(), Math.min(this.maximum(), t.current)), this.reset(t.current)
        }
    }, {
        filter: ["position"],
        run: function() {
            this.animate(this.coordinates(this._current))
        }
    }, {
        filter: ["width", "position", "items", "settings"],
        run: function() {
            var t, e, i, n, r = this.settings.rtl ? 1 : -1,
                s = 2 * this.settings.stagePadding,
                o = this.coordinates(this.current()) + s,
                a = o + this.width() * r,
                l = [];
            for (i = 0, n = this._coordinates.length; i < n; i++) t = this._coordinates[i - 1] || 0, e = Math.abs(this._coordinates[i]) + s * r, (this.op(t, "<=", o) && this.op(t, ">", a) || this.op(e, "<", o) && this.op(e, ">", a)) && l.push(i);
            this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + l.join("), :eq(") + ")").addClass("active"), this.settings.center && (this.$stage.children(".center").removeClass("center"), this.$stage.children().eq(this.current()).addClass("center"))
        }
    }], r.prototype.initialize = function() {
        var e, i, r;
        (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) && (e = this.$element.find("img"), i = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : n, r = this.$element.children(i).width(), e.length && r <= 0 && this.preloadAutoWidthImages(e));
        this.$element.addClass(this.options.loadingClass), this.$stage = t("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'), this.$element.append(this.$stage.parent()), this.replace(this.$element.children().not(this.$stage.parent())), this.$element.is(":visible") ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized")
    }, r.prototype.setup = function() {
        var e = this.viewport(),
            i = this.options.responsive,
            n = -1,
            r = null;
        i ? (t.each(i, function(t) {
            t <= e && t > n && (n = Number(t))
        }), "function" == typeof(r = t.extend({}, this.options, i[n])).stagePadding && (r.stagePadding = r.stagePadding()), delete r.responsive, r.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + n))) : r = t.extend({}, this.options), this.trigger("change", {
            property: {
                name: "settings",
                value: r
            }
        }), this._breakpoint = n, this.settings = r, this.invalidate("settings"), this.trigger("changed", {
            property: {
                name: "settings",
                value: this.settings
            }
        })
    }, r.prototype.optionsLogic = function() {
        this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1)
    }, r.prototype.prepare = function(e) {
        var i = this.trigger("prepare", {
            content: e
        });
        return i.data || (i.data = t("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(e)), this.trigger("prepared", {
            content: i.data
        }), i.data
    }, r.prototype.update = function() {
        for (var e = 0, i = this._pipe.length, n = t.proxy(function(t) {
                return this[t]
            }, this._invalidated), r = {}; e < i;)(this._invalidated.all || t.grep(this._pipe[e].filter, n).length > 0) && this._pipe[e].run(r), e++;
        this._invalidated = {}, !this.is("valid") && this.enter("valid")
    }, r.prototype.width = function(t) {
        switch (t = t || r.Width.Default) {
            case r.Width.Inner:
            case r.Width.Outer:
                return this._width;
            default:
                return this._width - 2 * this.settings.stagePadding + this.settings.margin
        }
    }, r.prototype.refresh = function() {
        this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed")
    }, r.prototype.onThrottledResize = function() {
        e.clearTimeout(this.resizeTimer), this.resizeTimer = e.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
    }, r.prototype.onResize = function() {
        return !!this._items.length && this._width !== this.$element.width() && !!this.$element.is(":visible") && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")))
    }, r.prototype.registerEventHandlers = function() {
        t.support.transition && this.$stage.on(t.support.transition.end + ".owl.core", t.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(e, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function() {
            return !1
        })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", t.proxy(this.onDragEnd, this)))
    }, r.prototype.onDragStart = function(e) {
        var n = null;
        3 !== e.which && (t.support.transform ? n = {
            x: (n = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","))[16 === n.length ? 12 : 4],
            y: n[16 === n.length ? 13 : 5]
        } : (n = this.$stage.position(), n = {
            x: this.settings.rtl ? n.left + this.$stage.width() - this.width() + this.settings.margin : n.left,
            y: n.top
        }), this.is("animating") && (t.support.transform ? this.animate(n.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === e.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = t(e.target), this._drag.stage.start = n, this._drag.stage.current = n, this._drag.pointer = this.pointer(e), t(i).on("mouseup.owl.core touchend.owl.core", t.proxy(this.onDragEnd, this)), t(i).one("mousemove.owl.core touchmove.owl.core", t.proxy(function(e) {
            var n = this.difference(this._drag.pointer, this.pointer(e));
            t(i).on("mousemove.owl.core touchmove.owl.core", t.proxy(this.onDragMove, this)), Math.abs(n.x) < Math.abs(n.y) && this.is("valid") || (e.preventDefault(), this.enter("dragging"), this.trigger("drag"))
        }, this)))
    }, r.prototype.onDragMove = function(t) {
        var e = null,
            i = null,
            n = null,
            r = this.difference(this._drag.pointer, this.pointer(t)),
            s = this.difference(this._drag.stage.start, r);
        this.is("dragging") && (t.preventDefault(), this.settings.loop ? (e = this.coordinates(this.minimum()), i = this.coordinates(this.maximum() + 1) - e, s.x = ((s.x - e) % i + i) % i + e) : (e = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), i = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), n = this.settings.pullDrag ? -1 * r.x / 5 : 0, s.x = Math.max(Math.min(s.x, e + n), i + n)), this._drag.stage.current = s, this.animate(s.x))
    }, r.prototype.onDragEnd = function(e) {
        var n = this.difference(this._drag.pointer, this.pointer(e)),
            r = this._drag.stage.current,
            s = n.x > 0 ^ this.settings.rtl ? "left" : "right";
        t(i).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== n.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(r.x, 0 !== n.x ? s : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = s, (Math.abs(n.x) > 3 || (new Date).getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function() {
            return !1
        })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"))
    }, r.prototype.closest = function(e, i) {
        var n = -1,
            r = this.width(),
            s = this.coordinates();
        return this.settings.freeDrag || t.each(s, t.proxy(function(t, o) {
            return "left" === i && e > o - 30 && e < o + 30 ? n = t : "right" === i && e > o - r - 30 && e < o - r + 30 ? n = t + 1 : this.op(e, "<", o) && this.op(e, ">", s[t + 1] || o - r) && (n = "left" === i ? t + 1 : t), -1 === n
        }, this)), this.settings.loop || (this.op(e, ">", s[this.minimum()]) ? n = e = this.minimum() : this.op(e, "<", s[this.maximum()]) && (n = e = this.maximum())), n
    }, r.prototype.animate = function(e) {
        var i = this.speed() > 0;
        this.is("animating") && this.onTransitionEnd(), i && (this.enter("animating"), this.trigger("translate")), t.support.transform3d && t.support.transition ? this.$stage.css({
            transform: "translate3d(" + e + "px,0px,0px)",
            transition: this.speed() / 1e3 + "s"
        }) : i ? this.$stage.animate({
            left: e + "px"
        }, this.speed(), this.settings.fallbackEasing, t.proxy(this.onTransitionEnd, this)) : this.$stage.css({
            left: e + "px"
        })
    }, r.prototype.is = function(t) {
        return this._states.current[t] && this._states.current[t] > 0
    }, r.prototype.current = function(t) {
        if (t === n) return this._current;
        if (0 === this._items.length) return n;
        if (t = this.normalize(t), this._current !== t) {
            var e = this.trigger("change", {
                property: {
                    name: "position",
                    value: t
                }
            });
            e.data !== n && (t = this.normalize(e.data)), this._current = t, this.invalidate("position"), this.trigger("changed", {
                property: {
                    name: "position",
                    value: this._current
                }
            })
        }
        return this._current
    }, r.prototype.invalidate = function(e) {
        return "string" === t.type(e) && (this._invalidated[e] = !0, this.is("valid") && this.leave("valid")), t.map(this._invalidated, function(t, e) {
            return e
        })
    }, r.prototype.reset = function(t) {
        (t = this.normalize(t)) !== n && (this._speed = 0, this._current = t, this.suppress(["translate", "translated"]), this.animate(this.coordinates(t)), this.release(["translate", "translated"]))
    }, r.prototype.normalize = function(t, e) {
        var i = this._items.length,
            r = e ? 0 : this._clones.length;
        return !this.isNumeric(t) || i < 1 ? t = n : (t < 0 || t >= i + r) && (t = ((t - r / 2) % i + i) % i + r / 2), t
    }, r.prototype.relative = function(t) {
        return t -= this._clones.length / 2, this.normalize(t, !0)
    }, r.prototype.maximum = function(t) {
        var e, i, n, r = this.settings,
            s = this._coordinates.length;
        if (r.loop) s = this._clones.length / 2 + this._items.length - 1;
        else if (r.autoWidth || r.merge) {
            for (e = this._items.length, i = this._items[--e].width(), n = this.$element.width(); e-- && !((i += this._items[e].width() + this.settings.margin) > n););
            s = e + 1
        } else s = r.center ? this._items.length - 1 : this._items.length - r.items;
        return t && (s -= this._clones.length / 2), Math.max(s, 0)
    }, r.prototype.minimum = function(t) {
        return t ? 0 : this._clones.length / 2
    }, r.prototype.items = function(t) {
        return t === n ? this._items.slice() : (t = this.normalize(t, !0), this._items[t])
    }, r.prototype.mergers = function(t) {
        return t === n ? this._mergers.slice() : (t = this.normalize(t, !0), this._mergers[t])
    }, r.prototype.clones = function(e) {
        var i = this._clones.length / 2,
            r = i + this._items.length,
            s = function(t) {
                return t % 2 == 0 ? r + t / 2 : i - (t + 1) / 2
            };
        return e === n ? t.map(this._clones, function(t, e) {
            return s(e)
        }) : t.map(this._clones, function(t, i) {
            return t === e ? s(i) : null
        })
    }, r.prototype.speed = function(t) {
        return t !== n && (this._speed = t), this._speed
    }, r.prototype.coordinates = function(e) {
        var i, r = 1,
            s = e - 1;
        return e === n ? t.map(this._coordinates, t.proxy(function(t, e) {
            return this.coordinates(e)
        }, this)) : (this.settings.center ? (this.settings.rtl && (r = -1, s = e + 1), i = this._coordinates[e], i += (this.width() - i + (this._coordinates[s] || 0)) / 2 * r) : i = this._coordinates[s] || 0, i = Math.ceil(i))
    }, r.prototype.duration = function(t, e, i) {
        return 0 === i ? 0 : Math.min(Math.max(Math.abs(e - t), 1), 6) * Math.abs(i || this.settings.smartSpeed)
    }, r.prototype.to = function(t, e) {
        var i = this.current(),
            n = null,
            r = t - this.relative(i),
            s = (r > 0) - (r < 0),
            o = this._items.length,
            a = this.minimum(),
            l = this.maximum();
        this.settings.loop ? (!this.settings.rewind && Math.abs(r) > o / 2 && (r += -1 * s * o), (n = (((t = i + r) - a) % o + o) % o + a) !== t && n - r <= l && n - r > 0 && (i = n - r, t = n, this.reset(i))) : this.settings.rewind ? t = (t % (l += 1) + l) % l : t = Math.max(a, Math.min(l, t)), this.speed(this.duration(i, t, e)), this.current(t), this.$element.is(":visible") && this.update()
    }, r.prototype.next = function(t) {
        t = t || !1, this.to(this.relative(this.current()) + 1, t)
    }, r.prototype.prev = function(t) {
        t = t || !1, this.to(this.relative(this.current()) - 1, t)
    }, r.prototype.onTransitionEnd = function(t) {
        if (t !== n && (t.stopPropagation(), (t.target || t.srcElement || t.originalTarget) !== this.$stage.get(0))) return !1;
        this.leave("animating"), this.trigger("translated")
    }, r.prototype.viewport = function() {
        var n;
        return this.options.responsiveBaseElement !== e ? n = t(this.options.responsiveBaseElement).width() : e.innerWidth ? n = e.innerWidth : i.documentElement && i.documentElement.clientWidth ? n = i.documentElement.clientWidth : console.warn("Can not detect viewport width."), n
    }, r.prototype.replace = function(e) {
        this.$stage.empty(), this._items = [], e && (e = e instanceof jQuery ? e : t(e)), this.settings.nestedItemSelector && (e = e.find("." + this.settings.nestedItemSelector)), e.filter(function() {
            return 1 === this.nodeType
        }).each(t.proxy(function(t, e) {
            e = this.prepare(e), this.$stage.append(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)
        }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items")
    }, r.prototype.add = function(e, i) {
        var r = this.relative(this._current);
        i = i === n ? this._items.length : this.normalize(i, !0), e = e instanceof jQuery ? e : t(e), this.trigger("add", {
            content: e,
            position: i
        }), e = this.prepare(e), 0 === this._items.length || i === this._items.length ? (0 === this._items.length && this.$stage.append(e), 0 !== this._items.length && this._items[i - 1].after(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[i].before(e), this._items.splice(i, 0, e), this._mergers.splice(i, 0, 1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[r] && this.reset(this._items[r].index()), this.invalidate("items"), this.trigger("added", {
            content: e,
            position: i
        })
    }, r.prototype.remove = function(t) {
        (t = this.normalize(t, !0)) !== n && (this.trigger("remove", {
            content: this._items[t],
            position: t
        }), this._items[t].remove(), this._items.splice(t, 1), this._mergers.splice(t, 1), this.invalidate("items"), this.trigger("removed", {
            content: null,
            position: t
        }))
    }, r.prototype.preloadAutoWidthImages = function(e) {
        e.each(t.proxy(function(e, i) {
            this.enter("pre-loading"), i = t(i), t(new Image).one("load", t.proxy(function(t) {
                i.attr("src", t.target.src), i.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh()
            }, this)).attr("src", i.attr("src") || i.attr("data-src") || i.attr("data-src-retina"))
        }, this))
    }, r.prototype.destroy = function() {
        for (var n in this.$element.off(".owl.core"), this.$stage.off(".owl.core"), t(i).off(".owl.core"), !1 !== this.settings.responsive && (e.clearTimeout(this.resizeTimer), this.off(e, "resize", this._handlers.onThrottledResize)), this._plugins) this._plugins[n].destroy();
        this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel")
    }, r.prototype.op = function(t, e, i) {
        var n = this.settings.rtl;
        switch (e) {
            case "<":
                return n ? t > i : t < i;
            case ">":
                return n ? t < i : t > i;
            case ">=":
                return n ? t <= i : t >= i;
            case "<=":
                return n ? t >= i : t <= i
        }
    }, r.prototype.on = function(t, e, i, n) {
        t.addEventListener ? t.addEventListener(e, i, n) : t.attachEvent && t.attachEvent("on" + e, i)
    }, r.prototype.off = function(t, e, i, n) {
        t.removeEventListener ? t.removeEventListener(e, i, n) : t.detachEvent && t.detachEvent("on" + e, i)
    }, r.prototype.trigger = function(e, i, n, s, o) {
        var a = {
                item: {
                    count: this._items.length,
                    index: this.current()
                }
            },
            l = t.camelCase(t.grep(["on", e, n], function(t) {
                return t
            }).join("-").toLowerCase()),
            h = t.Event([e, "owl", n || "carousel"].join(".").toLowerCase(), t.extend({
                relatedTarget: this
            }, a, i));
        return this._supress[e] || (t.each(this._plugins, function(t, e) {
            e.onTrigger && e.onTrigger(h)
        }), this.register({
            type: r.Type.Event,
            name: e
        }), this.$element.trigger(h), this.settings && "function" == typeof this.settings[l] && this.settings[l].call(this, h)), h
    }, r.prototype.enter = function(e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function(t, e) {
            this._states.current[e] === n && (this._states.current[e] = 0), this._states.current[e]++
        }, this))
    }, r.prototype.leave = function(e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function(t, e) {
            this._states.current[e]--
        }, this))
    }, r.prototype.register = function(e) {
        if (e.type === r.Type.Event) {
            if (t.event.special[e.name] || (t.event.special[e.name] = {}), !t.event.special[e.name].owl) {
                var i = t.event.special[e.name]._default;
                t.event.special[e.name]._default = function(t) {
                    return !i || !i.apply || t.namespace && -1 !== t.namespace.indexOf("owl") ? t.namespace && t.namespace.indexOf("owl") > -1 : i.apply(this, arguments)
                }, t.event.special[e.name].owl = !0
            }
        } else e.type === r.Type.State && (this._states.tags[e.name] ? this._states.tags[e.name] = this._states.tags[e.name].concat(e.tags) : this._states.tags[e.name] = e.tags, this._states.tags[e.name] = t.grep(this._states.tags[e.name], t.proxy(function(i, n) {
            return t.inArray(i, this._states.tags[e.name]) === n
        }, this)))
    }, r.prototype.suppress = function(e) {
        t.each(e, t.proxy(function(t, e) {
            this._supress[e] = !0
        }, this))
    }, r.prototype.release = function(e) {
        t.each(e, t.proxy(function(t, e) {
            delete this._supress[e]
        }, this))
    }, r.prototype.pointer = function(t) {
        var i = {
            x: null,
            y: null
        };
        return (t = (t = t.originalEvent || t || e.event).touches && t.touches.length ? t.touches[0] : t.changedTouches && t.changedTouches.length ? t.changedTouches[0] : t).pageX ? (i.x = t.pageX, i.y = t.pageY) : (i.x = t.clientX, i.y = t.clientY), i
    }, r.prototype.isNumeric = function(t) {
        return !isNaN(parseFloat(t))
    }, r.prototype.difference = function(t, e) {
        return {
            x: t.x - e.x,
            y: t.y - e.y
        }
    }, t.fn.owlCarousel = function(e) {
        var i = Array.prototype.slice.call(arguments, 1);
        return this.each(function() {
            var n = t(this),
                s = n.data("owl.carousel");
            s || (s = new r(this, "object" == typeof e && e), n.data("owl.carousel", s), t.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function(e, i) {
                s.register({
                    type: r.Type.Event,
                    name: i
                }), s.$element.on(i + ".owl.carousel.core", t.proxy(function(t) {
                    t.namespace && t.relatedTarget !== this && (this.suppress([i]), s[i].apply(this, [].slice.call(arguments, 1)), this.release([i]))
                }, s))
            })), "string" == typeof e && "_" !== e.charAt(0) && s[e].apply(s, i)
        })
    }, t.fn.owlCarousel.Constructor = r
}(window.Zepto || window.jQuery, window, document),
function(t, e, i, n) {}(window.Zepto || window.jQuery, window, document),

// from here
function(t, e, i, n) {}(window.Zepto || window.jQuery, window, document),
function(t, e, i, n) {}(window.Zepto || window.jQuery, window, document),
function(t, e, i, n) {
    var r = function(e) {};
    r.Defaults = {}, r.prototype.fetch = function(t, e) {}, r.prototype.thumbnail = function(e, i) {}, r.prototype.stop = function() {}, r.prototype.play = function(e) {
            var i, n = t(e.target).closest("." + this._core.settings.itemClass),
                r = this._videos[n.attr("data-video")],
                s = r.width || "100%",
                o = r.height || this._core.$stage.height();
            this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), n = this._core.items(this._core.relative(n.index())), this._core.reset(n.index()), "youtube" === r.type ? i = '<iframe width="' + s + '" height="' + o + '" src="//www.youtube.com/embed/' + r.id + "?autoplay=1&rel=0&v=" + r.id + '" frameborder="0" allowfullscreen></iframe>' : "vimeo" === r.type ? i = '<iframe src="//player.vimeo.com/video/' + r.id + '?autoplay=1" width="' + s + '" height="' + o + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' : "vzaar" === r.type && (i = '<iframe frameborder="0"height="' + o + '"width="' + s + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/' + r.id + '/player?autoplay=true"></iframe>'), t('<div class="owl-video-frame">' + i + "</div>").insertAfter(n.find(".owl-video")), this._playing = n.addClass("owl-video-playing"))
        }, r.prototype.isInFullScreen = function() {},
        // from here
        r.prototype.destroy = function() {}, t.fn.owlCarousel.Constructor.Plugins.Video = r
}(window.Zepto || window.jQuery, window, document),
function(t, e, i, n) {}(window.Zepto || window.jQuery, window, document),
function(t, e, i, n) {}(window.Zepto || window.jQuery, window, document),
function(t, e, i, n) {}(window.Zepto || window.jQuery, window, document),
function(t, e, i, n) {}(window.Zepto || window.jQuery, window, document),
function(t, e, i, n) {
    function r(e, i) {}

    function s(t) {}
    var o = t("<support>").get(0).style,
        a = "Webkit Moz O ms".split(" "),
        l = {},
        h = function() {
            return !!r("transform")
        },
        c = function() {
            return !!r("perspective")
        },
        u = function() {
            return !!r("animation")
        };
    (function() {})() && (t.support.transition = new String(s("transition")), t.support.transition.end = l.transition.end[t.support.transition]), u() && (t.support.animation = new String(s("animation")), t.support.animation.end = l.animation.end[t.support.animation]), h() && (t.support.transform = new String(s("transform")), t.support.transform3d = c())
}(window.Zepto || window.jQuery, window, document),
function(t) {}(function(t) {}),
function(t, e) {
    "function" == typeof define && define.amd ? define(e) : "object" == typeof exports ? module.exports = e() : t.Blazy = e()
}(this, function() {
    function t(t) {
        var i = t._util;
        i.elements = function(t) {
            for (var e = [], i = (t = t.root.querySelectorAll(t.selector)).length; i--; e.unshift(t[i]));
            return e
        }(t.options), i.count = i.elements.length, i.destroyed && (i.destroyed = !1, t.options.container && u(t.options.container, function(t) {
            h(t, "scroll", i.validateT)
        }), h(window, "resize", i.saveViewportOffsetT), h(window, "resize", i.validateT), h(window, "scroll", i.validateT)), e(t)
    }

    function e(t) {
        for (var e = t._util, n = 0; n < e.count; n++) {
            var r, s = e.elements[n],
                a = s;
            r = t.options;
            var l = a.getBoundingClientRect();
            r.container && g && (a = a.closest(r.containerClass)) ? r = !!i(a = a.getBoundingClientRect(), f) && i(l, {
                top: a.top - r.offset,
                right: a.right + r.offset,
                bottom: a.bottom + r.offset,
                left: a.left - r.offset
            }) : r = i(l, f), (r || o(s, t.options.successClass)) && (t.load(s), e.elements.splice(n, 1), e.count--, n--)
        }
        0 === e.count && t.destroy()
    }

    function i(t, e) {
        return t.right >= e.left && t.bottom >= e.top && t.left <= e.right && t.top <= e.bottom
    }

    function n(t, e, i) {
        if (!o(t, i.successClass) && (e || i.loadInvisible || 0 < t.offsetWidth && 0 < t.offsetHeight))
            if (e = t.getAttribute(d) || t.getAttribute(i.src)) {
                var n = (e = e.split(i.separator))[m && 1 < e.length ? 1 : 0],
                    l = t.getAttribute(i.srcset),
                    p = "img" === t.nodeName.toLowerCase(),
                    f = (e = t.parentNode) && "picture" === e.nodeName.toLowerCase();
                if (p || void 0 === t.src) {
                    var g = new Image,
                        v = function() {
                            i.error && i.error(t, "invalid"), a(t, i.errorClass), c(g, "error", v), c(g, "load", _)
                        },
                        _ = function() {
                            p ? f || s(t, n, l) : t.style.backgroundImage = 'url("' + n + '")', r(t, i), c(g, "load", _), c(g, "error", v)
                        };
                    f && (g = t, u(e.getElementsByTagName("source"), function(t) {
                        var e = i.srcset,
                            n = t.getAttribute(e);
                        n && (t.setAttribute("srcset", n), t.removeAttribute(e))
                    })), h(g, "error", v), h(g, "load", _), s(g, n, l)
                } else t.src = n, r(t, i)
            } else "video" === t.nodeName.toLowerCase() ? (u(t.getElementsByTagName("source"), function(t) {
                var e = i.src,
                    n = t.getAttribute(e);
                n && (t.setAttribute("src", n), t.removeAttribute(e))
            }), t.load(), r(t, i)) : (i.error && i.error(t, "missing"), a(t, i.errorClass))
    }

    function r(t, e) {
        a(t, e.successClass), e.success && e.success(t), t.removeAttribute(e.src), t.removeAttribute(e.srcset), u(e.breakpoints, function(e) {
            t.removeAttribute(e.src)
        })
    }

    function s(t, e, i) {
        i && t.setAttribute("srcset", i), t.src = e
    }

    function o(t, e) {
        return -1 !== (" " + t.className + " ").indexOf(" " + e + " ")
    }

    function a(t, e) {
        o(t, e) || (t.className += " " + e)
    }

    function l(t) {
        f.bottom = (window.innerHeight || document.documentElement.clientHeight) + t, f.right = (window.innerWidth || document.documentElement.clientWidth) + t
    }

    function h(t, e, i) {
        t.attachEvent ? t.attachEvent && t.attachEvent("on" + e, i) : t.addEventListener(e, i, {
            capture: !1,
            passive: !0
        })
    }

    function c(t, e, i) {
        t.detachEvent ? t.detachEvent && t.detachEvent("on" + e, i) : t.removeEventListener(e, i, {
            capture: !1,
            passive: !0
        })
    }

    function u(t, e) {
        if (t && e)
            for (var i = t.length, n = 0; n < i && !1 !== e(t[n], n); n++);
    }

    function p(t, e, i) {
        var n = 0;
        return function() {
            var r = +new Date;
            r - n < e || (n = r, t.apply(i, arguments))
        }
    }
    var d, f, m, g;
    return function(i) {
        if (!document.querySelectorAll) {
            var r = document.createStyleSheet();
            document.querySelectorAll = function(t, e, i, n, s) {
                for (s = document.all, e = [], i = (t = t.replace(/\[for\b/gi, "[htmlFor").split(",")).length; i--;) {
                    for (r.addRule(t[i], "k:v"), n = s.length; n--;) s[n].currentStyle.k && e.push(s[n]);
                    r.removeRule(0)
                }
                return e
            }
        }
        var s = this,
            o = s._util = {};
        o.elements = [], o.destroyed = !0, s.options = i || {}, s.options.error = s.options.error || !1, s.options.offset = s.options.offset || 100, s.options.root = s.options.root || document, s.options.success = s.options.success || !1, s.options.selector = s.options.selector || ".b-lazy", s.options.separator = s.options.separator || "|", s.options.containerClass = s.options.container, s.options.container = !!s.options.containerClass && document.querySelectorAll(s.options.containerClass), s.options.errorClass = s.options.errorClass || "b-error", s.options.breakpoints = s.options.breakpoints || !1, s.options.loadInvisible = s.options.loadInvisible || !1, s.options.successClass = s.options.successClass || "b-loaded", s.options.validateDelay = s.options.validateDelay || 25, s.options.saveViewportOffsetDelay = s.options.saveViewportOffsetDelay || 50, s.options.srcset = s.options.srcset || "data-srcset", s.options.src = d = s.options.src || "data-src", g = Element.prototype.closest, m = 1 < window.devicePixelRatio, (f = {}).top = 0 - s.options.offset, f.left = 0 - s.options.offset, s.revalidate = function() {
            t(s)
        }, s.load = function(t, e) {
            var i = this.options;
            void 0 === t.length ? n(t, e, i) : u(t, function(t) {
                n(t, e, i)
            })
        }, s.destroy = function() {
            var t = this._util;
            this.options.container && u(this.options.container, function(e) {
                c(e, "scroll", t.validateT)
            }), c(window, "scroll", t.validateT), c(window, "resize", t.validateT), c(window, "resize", t.saveViewportOffsetT), t.count = 0, t.elements.length = 0, t.destroyed = !0
        }, o.validateT = p(function() {
            e(s)
        }, s.options.validateDelay, s), o.saveViewportOffsetT = p(function() {
            l(s.options.offset)
        }, s.options.saveViewportOffsetDelay, s), l(s.options.offset), u(s.options.breakpoints, function(t) {
            if (t.width >= window.screen.width) return d = t.src, !1
        }), setTimeout(function() {
            t(s)
        })
    }
}), console.log("%c Nice!! ", "background: #00F; color: #fff"), new Blazy;
var bodyResized = !1;
$(".menu-trigger").on("click", function() {}), $(".overlay").on("click", function() {}), $(window).on("scroll load", function() {
    $(window).scrollTop() > 0 ? $("nav").addClass("scrolled") : $("nav").removeClass("scrolled")
});
var resizeBody = function() {};
$(".popup-dot").on("click", function(t) {}), $(window).on("click", function() {});
var $graphDots = $(".graph-dot"),
    $journeyCol = $(".journey-column");
$graphDots.on("click", function() {}), $journeyCol.on("click", function() {}),
    $(".gallery-button, .gallery-right-overlay").on("click", function(t) {
        t.preventDefault(), $(".section.gallery, nav").addClass("gallery-view-active")
    }), $(".gallery-close-icon").on("click", function() {
        $(".section.gallery, nav").removeClass("gallery-view-active")
    }), $(".specs-close-icon").on("click", function() {
        $(".section.gallery, nav").removeClass("gallery-view-active")
    }), $(".gallery-carousel").owlCarousel({}), $(".specs-carousel").owlCarousel({}), $(".council-carousel").owlCarousel({}), $(".team-right").on("click", function(t) {
        if (!$(this).hasClass("animating") && null == $(t.target).attr("href")) {
            if ($(".team-right").addClass("animating"), switchTeamContent(), $(".button-next").hasClass("expanded")) {}
            $(".button-next.to-expand").addClass("expanded"), 0 != mobileTeamMembersCarousel && mobileTeamMembersCarousel.trigger("next.owl.carousel")
        }
    });
var switchTeamContent = function() {};
$(".scroller").on("click", function(t) {});
var currentStep = 1;
$(".mobile-journey-controls .arrow").on("click", function() {}), $(window).on("scroll", function() {}), $(window).on("load", function() {
    $("body").hasClass("mobile") && $(".slide.hero, .preloader-overlay.hero").css("height", $(window).height()), $(".preloader-overlay").addClass("active")
}), $(".opens-popup").on("click", function(t) {
    t.preventDefault(), $("body").addClass("popup-open");
    var e = $(this).attr("data-popup");
    if ($(".popup." + e).addClass("active"), $(".popup." + e).hasClass("video-popup")) {
        if ($(".slide.active").find("iframe").length > 0) {
            var i = parseInt($(".slide.active").find("iframe").attr("data-videocount"));
            players[i].pauseVideo()
        }
        var n = parseInt($(".popup." + e).find("iframe").attr("data-videocount"));
        players[n].playVideo()
    }
}), $(".popup").on("click", function(t) {}), $(".popup .gallery-close-icon").on("click", function() {
    closePopup()
});
var batteryCellsInterval, closePopup = function() {
        if ($("body").removeClass("popup-open"), $(".popup.active").hasClass("video-popup")) {
            var t = parseInt($(".popup.active").find("iframe").attr("data-videocount"));
            players[t].pauseVideo()
        }
        if ($(".slide.active").find("iframe").length > 0) {
            var e = parseInt($(".slide.active").find("iframe").attr("data-videocount"));
            players[e].playVideo()
        }
        $(".popup.active").removeClass("active")
    },
    createInterval = function() {
        batteryCellsInterval = window.setInterval(function() {}, 2e3)
    };
$(".nav-reserve").on("click", function() {});